/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-type.h"

G_DEFINE_ABSTRACT_TYPE (HilType, hil_type, HIL_TYPE_NODE)

static gboolean
is_compatible_with (HilType *source_type,
                    HilType *target_type)
{
    return FALSE;
}

static void
hil_type_class_init (HilTypeClass *klass)
{
    klass->is_compatible_with = is_compatible_with;
}

static void
hil_type_init (HilType *self)
{
    (void) self;
}

gboolean
hil_type_is_compatible_with (HilType *source_type,
                             HilType *target_type)
{
    g_return_val_if_fail (HIL_IS_TYPE (source_type), FALSE);
    g_return_val_if_fail (HIL_IS_TYPE (target_type), FALSE);

    if (source_type == target_type)
    {
        return TRUE;
    }

    return HIL_TYPE_GET_CLASS (source_type)->is_compatible_with (source_type,
                                                                 target_type);
}
