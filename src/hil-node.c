/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-node.h"

#include "hil-source-reference.h"
#include "hil-visitor.h"

typedef struct
{
    HilSourceReference *source_reference;
} HilNodePrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (HilNode, hil_node, G_TYPE_OBJECT)

enum
{
    PROP_SOURCE_REFERENCE = 1,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL };

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    HilNode *node;
    HilNodePrivate *priv;

    node = HIL_NODE (object);
    priv = hil_node_get_instance_private (node);

    switch (property_id)
    {
        case PROP_SOURCE_REFERENCE:
        {
            g_clear_object (&priv->source_reference);
            priv->source_reference = g_value_dup_object (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
    HilNode *node;
    HilNodePrivate *priv;

    node = HIL_NODE (object);
    priv = hil_node_get_instance_private (node);

    switch (property_id)
    {
        case PROP_SOURCE_REFERENCE:
        {
            g_value_set_object (value, priv->source_reference);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
finalize (GObject *object)
{
    HilNode *node;
    HilNodePrivate *priv;

    node = HIL_NODE (object);
    priv = hil_node_get_instance_private (node);

    g_clear_object (&priv->source_reference);
}

static void
accept (HilNode    *node,
        HilVisitor *visitor)
{
    (void) node;
    (void) visitor;
}

static void
accept_children (HilNode    *node,
                 HilVisitor *visitor)
{
    (void) node;
    (void) visitor;
}

static void
print (HilNode *node,
       gsize   *depth)
{
    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }

    g_print ("%s\n", g_type_name (G_TYPE_FROM_INSTANCE (node)));
}

static void
hil_node_class_init (HilNodeClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->set_property = set_property;
    object_class->get_property = get_property;
    object_class->finalize = finalize;

    klass->accept = accept;
    klass->accept_children = accept_children;
    klass->print = print;

    properties[PROP_SOURCE_REFERENCE] =
        g_param_spec_object ("source-reference", "Source reference", "Source reference",
                             HIL_TYPE_SOURCE_REFERENCE,
                             G_PARAM_CONSTRUCT_ONLY |
                             G_PARAM_READWRITE |
                             G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
hil_node_init (HilNode *self)
{
    (void) self;
}

HilSourceReference *
hil_node_get_source_reference (HilNode *node)
{
    HilNodePrivate *priv;

    g_return_val_if_fail (HIL_IS_NODE (node), NULL);

    priv = hil_node_get_instance_private (node);

    return g_object_ref (priv->source_reference);
}

void
hil_node_accept (HilNode    *node,
                 HilVisitor *visitor)
{
    g_return_if_fail (HIL_IS_NODE (node));
    g_return_if_fail (HIL_IS_VISITOR (visitor));

    HIL_NODE_GET_CLASS (node)->accept (node, visitor);
}

void
hil_node_accept_children (HilNode    *node,
                          HilVisitor *visitor)
{
    g_return_if_fail (HIL_IS_NODE (node));
    g_return_if_fail (HIL_IS_VISITOR (visitor));

    HIL_NODE_GET_CLASS (node)->accept_children (node, visitor);
}

void
hil_node_print (HilNode *node)
{
    HilNodeClass *node_class;
    gsize depth = 0;

    node_class = HIL_NODE_GET_CLASS (node);

    node_class->print (node, &depth);
}
