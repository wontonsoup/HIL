/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-function.h"

#include "hil-callable-type.h"
#include "hil-compound-statement.h"
#include "hil-subroutine-parameter.h"
#include "hil-value-type.h"
#include "hil-visitor.h"

struct _HilFunction
{
    HilDeclaration parent_instance;

    GList *parameters;
    HilCompoundStatement *body;
    HilType *return_type;
};

G_DEFINE_TYPE (HilFunction, hil_function,
               HIL_TYPE_DECLARATION)

static void
finalize (GObject *object)
{
    HilFunction *function;

    function = HIL_FUNCTION (object);

    g_list_free_full (function->parameters, g_object_unref);
    g_clear_object (&function->body);
    g_clear_object (&function->return_type);

    G_OBJECT_CLASS (hil_function_parent_class)->finalize (object);
}

static void
accept (HilNode    *node,
        HilVisitor *visitor)
{
    HIL_NODE_CLASS (hil_function_parent_class)->accept (node, visitor);

    hil_visitor_visit_function (visitor, HIL_FUNCTION (node));
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilFunction *function;
    HilDeclaration *declaration;
    g_autofree gchar *identifier = NULL;
    HilNode *return_type;
    HilNode *body = NULL;
    gsize current_depth;

    function = HIL_FUNCTION (node);
    declaration = HIL_DECLARATION (function);
    identifier = hil_declaration_get_identifier (declaration);
    return_type = HIL_NODE (function->return_type);
    if (function->body != NULL)
    {
        body = HIL_NODE (function->body);
    }

    HIL_NODE_CLASS (hil_function_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Identifier: %s\n", identifier);

    if (function->parameters != NULL)
    {
        for (gsize i = *depth; i > 0; i--)
        {
            g_print ("  ");
        }
        g_print ("Parameters:\n");

        (*depth)++;

        for (GList *i = function->parameters; i != NULL; i = i->next)
        {
            g_autofree gchar *parameter_name = NULL;
            g_autofree gchar *parameter_type = NULL;

            parameter_name = hil_subroutine_parameter_get_identifier (i->data);
            parameter_type = hil_subroutine_parameter_get_parameter_type (i->data);

            for (gsize i = *depth; i > 0; i--)
            {
                g_print ("  ");
            }
            g_print ("%s: %s\n", parameter_name, parameter_type);
        }

        (*depth)--;
    }

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Return type:\n");

    current_depth = *depth + 1;

    HIL_NODE_GET_CLASS (return_type)->print (return_type, &current_depth);

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Body:\n");

    (*depth)++;

    if (body != NULL)
    {
        HIL_NODE_GET_CLASS (body)->print (body, depth);
    }
}

static void
hil_function_class_init (HilFunctionClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->accept = accept;
    node_class->print = print;
}

static void
hil_function_init (HilFunction *self)
{
    (void) self;
}

HilNode *
hil_function_new (const gchar          *identifier,
                  GList                *parameters,
                  HilType              *return_type,
                  HilCompoundStatement *body)
{
    HilFunction *function;

    g_return_val_if_fail (HIL_IS_COMPOUND_STATEMENT (body), NULL);
    g_return_val_if_fail (HIL_IS_TYPE (return_type), NULL);

    function = g_object_new (HIL_TYPE_FUNCTION,
                             "identifier", identifier,
                             "type", hil_callable_type_new (return_type),
                             NULL);

    function->parameters = parameters;
    function->body = g_object_ref (body);
    function->return_type = g_object_ref (return_type);

    return HIL_NODE (function);
}
