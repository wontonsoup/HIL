/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-void-type.h"

struct _HilVoidType
{
    HilType parent_instance;
};

G_DEFINE_TYPE (HilVoidType, hil_void_type, HIL_TYPE_TYPE)

static void
hil_void_type_class_init (HilVoidTypeClass *klass)
{
    (void) klass;
}

static void
hil_void_type_init (HilVoidType *self)
{
    (void) self;
}

HilNode *
hil_void_type_new (void)
{
    return g_object_new (HIL_TYPE_VOID_TYPE, NULL);
}
