/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-integer-literal.h"

#include "hil-integer-type.h"
#include "hil-source-reference.h"
#include "hil-visitor.h"

struct _HilIntegerLiteral
{
    HilLiteral parent_instance;
};

G_DEFINE_TYPE (HilIntegerLiteral, hil_integer_literal, HIL_TYPE_LITERAL)

static void
accept (HilNode    *node,
        HilVisitor *visitor)
{
    hil_visitor_visit_integer_literal (visitor, HIL_INTEGER_LITERAL (node));
}

static void
hil_integer_literal_class_init (HilIntegerLiteralClass *klass)
{
    HilNodeClass *node_class;

    node_class = HIL_NODE_CLASS (klass);

    node_class->accept = accept;
}

static void
hil_integer_literal_init (HilIntegerLiteral *self)
{
    (void) self;
}

HilNode *
hil_integer_literal_new (const gchar                *value,
                         struct _HilSourceReference *source_reference)
{
    HilIntegerLiteral *integer_literal;

    integer_literal = g_object_new (HIL_TYPE_INTEGER_LITERAL,
                                    "value", value,
                                    "expression-type", hil_integer_type_new (),
                                    "source-reference", source_reference,
                                    NULL);

    return HIL_NODE (integer_literal);
}
