/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-conditional-statement.h"

#include "hil-expression.h"

typedef struct
{
    HilExpression *condition;
} HilConditionalStatementPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (HilConditionalStatement, hil_conditional_statement,
                                     HIL_TYPE_STATEMENT)

enum
{
    PROP_CONDITION = 1,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL };

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    HilConditionalStatement *conditional_statement;
    HilConditionalStatementPrivate *priv;

    conditional_statement = HIL_CONDITIONAL_STATEMENT (object);
    priv = hil_conditional_statement_get_instance_private (conditional_statement);

    switch (property_id)
    {
        case PROP_CONDITION:
        {
            g_clear_object (&priv->condition);
            priv->condition = g_value_dup_object (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
    HilConditionalStatement *conditional_statement;
    HilConditionalStatementPrivate *priv;

    conditional_statement = HIL_CONDITIONAL_STATEMENT (object);
    priv = hil_conditional_statement_get_instance_private (conditional_statement);

    switch (property_id)
    {
        case PROP_CONDITION:
        {
            g_value_set_object (value, priv->condition);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
finalize (GObject *object)
{
    HilConditionalStatement *conditional_statement;
    HilConditionalStatementPrivate *priv;

    conditional_statement = HIL_CONDITIONAL_STATEMENT (object);
    priv = hil_conditional_statement_get_instance_private (conditional_statement);

    g_clear_object (&priv->condition);

    G_OBJECT_CLASS (hil_conditional_statement_parent_class)->finalize (object);
}

static void
hil_conditional_statement_class_init (HilConditionalStatementClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->set_property = set_property;
    object_class->get_property = get_property;
    object_class->finalize = finalize;

    properties[PROP_CONDITION] =
        g_param_spec_object ("condition", "Condition", "Condition",
                             HIL_TYPE_EXPRESSION,
                             G_PARAM_CONSTRUCT_ONLY | G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
hil_conditional_statement_init (HilConditionalStatement *self)
{
    (void) self;
}

HilNode *
hil_conditional_statement_get_condition (HilConditionalStatement *conditional_statement)
{
    HilConditionalStatementPrivate *priv;

    priv = hil_conditional_statement_get_instance_private (conditional_statement);

    g_return_val_if_fail (HIL_IS_CONDITIONAL_STATEMENT (conditional_statement), NULL);

    return g_object_ref (priv->condition);
}
