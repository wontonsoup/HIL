/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-binary-expression.h"

#include "hil-source-reference.h"
#include "hil-visitor.h"

struct _HilBinaryExpression
{
    HilExpression parent_instance;

    HilBinaryOperator binary_operator;

    HilExpression *lhs_expression;
    HilExpression *rhs_expression;
};

G_DEFINE_TYPE (HilBinaryExpression, hil_binary_expression, HIL_TYPE_EXPRESSION)

static void
finalize (GObject *object)
{
    HilBinaryExpression *binary_expression;

    binary_expression = HIL_BINARY_EXPRESSION (object);

    g_clear_object (&binary_expression->lhs_expression);
    g_clear_object (&binary_expression->rhs_expression);

    G_OBJECT_CLASS (hil_binary_expression_parent_class)->finalize (object);
}

static const gchar *
operator_to_string (HilBinaryOperator operator)
{
    switch (operator)
    {
        case HIL_BINARY_OPERATOR_ADDITION:
        {
            return "+";
        }
        break;

        case HIL_BINARY_OPERATOR_DIVISION:
        {
            return "/";
        }
        break;

        case HIL_BINARY_OPERATOR_EQUALITY:
        {
            return "==";
        }
        break;

        case HIL_BINARY_OPERATOR_GREATER_THAN:
        {
            return ">";
        }
        break;

        case HIL_BINARY_OPERATOR_GREATER_THAN_OR_EQUAL:
        {
            return ">=";
        }
        break;

        case HIL_BINARY_OPERATOR_INEQUALITY:
        {
            return "!=";
        }
        break;

        case HIL_BINARY_OPERATOR_LOGICAL_CONJUNCTION:
        {
            return "and";
        }
        break;

        case HIL_BINARY_OPERATOR_LOGICAL_DISJUNCTION:
        {
            return "or";
        }
        break;

        case HIL_BINARY_OPERATOR_LESS_THAN:
        {
            return "<";
        }
        break;

        case HIL_BINARY_OPERATOR_LESS_THAN_OR_EQUAL:
        {
            return "<=";
        }
        break;

        case HIL_BINARY_OPERATOR_MULTIPLICATION:
        {
            return "*";
        }
        break;

        case HIL_BINARY_OPERATOR_SUBTRACTION:
        {
            return "-";
        }
        break;
    }
}

static void
accept (HilNode    *node,
        HilVisitor *visitor)
{
    hil_visitor_visit_binary_expression (visitor, HIL_BINARY_EXPRESSION (node));
}

static void
accept_children (HilNode    *node,
                 HilVisitor *visitor)
{
    HilBinaryExpression *binary_expression;
    HilNode *lhs_expression;
    HilNode *rhs_expression;

    binary_expression = HIL_BINARY_EXPRESSION (node);
    lhs_expression = HIL_NODE (binary_expression->lhs_expression);
    rhs_expression = HIL_NODE (binary_expression->rhs_expression);

    hil_node_accept (lhs_expression, visitor);
    hil_node_accept (rhs_expression, visitor);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilBinaryExpression *binary_expression;
    HilNode *lhs_expression;
    HilNode *rhs_expression;
    gsize current_depth;

    binary_expression = HIL_BINARY_EXPRESSION (node);
    lhs_expression = HIL_NODE (binary_expression->lhs_expression);
    rhs_expression = HIL_NODE (binary_expression->rhs_expression);

    HIL_NODE_CLASS (hil_binary_expression_parent_class)->print (node, depth);

    (*depth)++;
    current_depth = *depth;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Operator: %s\n",
             operator_to_string (binary_expression->binary_operator));

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Left-hand-side expression:\n");

    (*depth)++;

    HIL_NODE_GET_CLASS (lhs_expression)->print (lhs_expression, depth);

    *depth = current_depth;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Right-hand-side expression:\n");

    (*depth)++;

    HIL_NODE_GET_CLASS (rhs_expression)->print (rhs_expression, depth);
}

static void
hil_binary_expression_class_init (HilBinaryExpressionClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->accept = accept;
    node_class->accept_children = accept_children;
    node_class->print = print;
}

static void
hil_binary_expression_init (HilBinaryExpression *self)
{
    (void) self;
}

HilExpression *
hil_binary_expression_get_lhs_expression (HilBinaryExpression *binary_expression)
{
    g_return_val_if_fail (HIL_IS_BINARY_EXPRESSION (binary_expression), NULL);

    return g_object_ref (binary_expression->lhs_expression);
}

HilExpression *
hil_binary_expression_get_rhs_expression (HilBinaryExpression *binary_expression)
{
    g_return_val_if_fail (HIL_IS_BINARY_EXPRESSION (binary_expression), NULL);

    return g_object_ref (binary_expression->rhs_expression);
}

gint
hil_binary_expression_get_operator (HilBinaryExpression *binary_expression)
{
    g_return_val_if_fail (HIL_IS_BINARY_EXPRESSION (binary_expression), -1);

    return binary_expression->binary_operator;
}

HilNode *
hil_binary_expression_new (HilBinaryOperator   binary_operator,
                           HilExpression      *lhs_expression,
                           HilExpression      *rhs_expression,
                           HilSourceReference *source_reference)
{
    HilBinaryExpression *binary_expression;

    g_return_val_if_fail (HIL_IS_EXPRESSION (lhs_expression), NULL);
    g_return_val_if_fail (HIL_IS_EXPRESSION (lhs_expression), NULL);

    binary_expression = g_object_new (HIL_TYPE_BINARY_EXPRESSION,
                                      "source-reference", source_reference,
                                      NULL);

    binary_expression->binary_operator = binary_operator;
    binary_expression->lhs_expression = g_object_ref (lhs_expression);
    binary_expression->rhs_expression = g_object_ref (rhs_expression);

    return HIL_NODE (binary_expression);
}
