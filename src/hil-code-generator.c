/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-code-generator.h"

#include "hil-binary-expression.h"
#include "hil-boolean-literal.h"
#include "hil-integer-literal.h"
#include "hil-real-literal.h"
#include "hil-source-file.h"
#include "hil-string-literal.h"
#include "hil-variable.h"

struct _HilCodeGenerator
{
    HilVisitor parent_instance;

    HilSourceFile *source_file;
};

G_DEFINE_TYPE (HilCodeGenerator, hil_code_generator, HIL_TYPE_VISITOR)

typedef enum
{
    HIL_INSTRUCTION_ADD,
    HIL_INSTRUCTION_DIV,
    HIL_INSTRUCTION_MUL,
    HIL_INSTRUCTION_SUB,

    HIL_INSTRUCTION_COMPARE_EQUAL,
    HIL_INSTRUCTION_COMPARE_GREATER_THAN,
    HIL_INSTRUCTION_COMPARE_GREATER_THAN_OR_EQUAL,
    HIL_INSTRUCTION_COMPARE_LESS_THAN,
    HIL_INSTRUCTION_COMPARE_LESS_THAN_OR_EQUAL,
    HIL_INSTRUCTION_COMPARE_NOT_EQUAL,

    HIL_INSTRUCTION_LOGICAL_CONJUNCTION,
    HIL_INSTRUCTION_LOGICAL_DISJUNCTION,

    HIL_INSTRUCTION_POP,
    HIL_INSTRUCTION_PUSH
} HilInstruction;

static void
visit_binary_expression (HilVisitor          *visitor,
                         HilBinaryExpression *binary_expression)
{
    HilCodeGenerator *code_generator;
    gint binary_operator;
    HilInstruction instruction;

    code_generator = HIL_CODE_GENERATOR (visitor);

    hil_node_accept_children (HIL_NODE (binary_expression), visitor);

    binary_operator = hil_binary_expression_get_operator (binary_expression);
    switch (binary_operator)
    {
        case HIL_BINARY_OPERATOR_ADDITION:
        {
            instruction = HIL_INSTRUCTION_ADD;
        }
        break;

        case HIL_BINARY_OPERATOR_DIVISION:
        {
            instruction = HIL_INSTRUCTION_DIV;
        }
        break;

        case HIL_BINARY_OPERATOR_EQUALITY:
        {
            instruction = HIL_INSTRUCTION_COMPARE_EQUAL;
        }
        break;

        case HIL_BINARY_OPERATOR_GREATER_THAN:
        {
            instruction = HIL_INSTRUCTION_COMPARE_GREATER_THAN;
        }
        break;

        case HIL_BINARY_OPERATOR_GREATER_THAN_OR_EQUAL:
        {
            instruction = HIL_INSTRUCTION_COMPARE_GREATER_THAN_OR_EQUAL;
        }
        break;

        case HIL_BINARY_OPERATOR_INEQUALITY:
        {
            instruction = HIL_INSTRUCTION_COMPARE_NOT_EQUAL;
        }
        break;

        case HIL_BINARY_OPERATOR_LOGICAL_CONJUNCTION:
        {
            instruction = HIL_INSTRUCTION_LOGICAL_CONJUNCTION;
        }
        break;

        case HIL_BINARY_OPERATOR_LOGICAL_DISJUNCTION:
        {
            instruction = HIL_INSTRUCTION_LOGICAL_DISJUNCTION;
        }
        break;

        case HIL_BINARY_OPERATOR_LESS_THAN:
        {
            instruction = HIL_INSTRUCTION_COMPARE_LESS_THAN;
        }
        break;

        case HIL_BINARY_OPERATOR_LESS_THAN_OR_EQUAL:
        {
            instruction = HIL_INSTRUCTION_COMPARE_LESS_THAN_OR_EQUAL;
        }
        break;

        case HIL_BINARY_OPERATOR_MULTIPLICATION:
        {
            instruction = HIL_INSTRUCTION_MUL;
        }
        break;

        case HIL_BINARY_OPERATOR_SUBTRACTION:
        {
            instruction = HIL_INSTRUCTION_SUB;
        }
        break;

        default:
        {
            g_assert_not_reached ();
        }
        break;
    }

    hil_source_file_emit (code_generator->source_file, instruction);
}

static void
visit_boolean_literal (HilVisitor        *visitor,
                       HilBooleanLiteral *boolean_literal)
{
}

static void
visit_integer_literal (HilVisitor        *visitor,
                       HilIntegerLiteral *integer_literal)
{
    g_autoptr (GVariant) variant = NULL;



    hil_source_file_emit2 (HIL_INSTRUCTION_PUSH, g_variant_new_int64 ());
}

static void
visit_real_literal (HilVisitor     *visitor,
                    HilRealLiteral *real_literal)
{
}

static void
visit_source_file (HilVisitor    *visitor,
                   HilSourceFile *source_file)
{
    HilCodeGenerator *code_generator;

    code_generator = HIL_CODE_GENERATOR (visitor);

    code_generator->source_file = source_file;

    hil_source_file_accept_children (source_file, visitor);
}

static void
visit_string_literal (HilVisitor       *visitor,
                      HilStringLiteral *string_literal)
{
    
}

static void
visit_variable (HilVisitor  *visitor,
                HilVariable *variable)
{
    g_autoptr (HilExpression) initializer = NULL;

    initializer = hil_variable_get_initializer (variable);

    hil_node_accept (HIL_NODE (initializer), visitor);
}

static void
hil_code_generator_class_init (HilCodeGeneratorClass *klass)
{
    HilVisitorClass *visitor_class;

    visitor_class = HIL_VISITOR_CLASS (klass);

    visitor_class->visit_binary_expression = visit_binary_expression;
    visitor_class->visit_boolean_literal = visit_boolean_literal;
    visitor_class->visit_integer_literal = visit_integer_literal;
    visitor_class->visit_real_literal = visit_real_literal;
    visitor_class->visit_source_file = visit_source_file;
    visitor_class->visit_string_literal = visit_string_literal;
    visitor_class->visit_variable = visit_variable;
}

static void
hil_code_generator_init (HilCodeGenerator *self)
{
    self->source_file = NULL;
}

void
hil_code_generator_emit (HilCodeGenerator *code_generator,
                         HilSourceFile    *source_file)
{
    g_return_if_fail (HIL_IS_CODE_GENERATOR (code_generator));
    g_return_if_fail (HIL_IS_SOURCE_FILE (source_file));

    hil_source_file_accept (source_file, HIL_VISITOR (code_generator));
}

HilCodeGenerator *
hil_code_generator_new (void)
{
    return g_object_new (HIL_TYPE_CODE_GENERATOR, NULL);
}
