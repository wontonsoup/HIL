/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-variable.h"

#include "hil-expression.h"
#include "hil-source-reference.h"
#include "hil-value-type.h"
#include "hil-visitor.h"

struct _HilVariable
{
    HilDeclaration parent_instance;

    HilExpression *initializer;
};

G_DEFINE_TYPE (HilVariable, hil_variable, HIL_TYPE_DECLARATION)

static void
finalize (GObject *object)
{
    HilVariable *variable;

    variable = HIL_VARIABLE (object);

    g_clear_object (&variable->initializer);

    G_OBJECT_CLASS (hil_variable_parent_class)->finalize (object);
}

static void
accept (HilNode    *node,
        HilVisitor *visitor)
{
    HIL_NODE_CLASS (hil_variable_parent_class)->accept (node, visitor);

    hil_visitor_visit_variable (visitor, HIL_VARIABLE (node));
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilVariable *variable;
    g_autofree gchar *identifier = NULL;
    g_autoptr (HilNode) type = NULL;
    HilNode *initializer;

    variable = HIL_VARIABLE (node);
    identifier = hil_declaration_get_identifier (HIL_DECLARATION (variable));
    type = HIL_NODE (hil_declaration_get_symbol_type (HIL_DECLARATION (variable)));
    initializer = HIL_NODE (variable->initializer);
    gsize current_depth;

    HIL_NODE_CLASS (hil_variable_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Identifier: %s\n", identifier);

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Type:\n");

    current_depth = *depth + 1;

    HIL_NODE_GET_CLASS (type)->print (type, &current_depth);

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Value:\n");

    (*depth)++;

    HIL_NODE_GET_CLASS (initializer)->print (initializer, depth);
}

static void
hil_variable_class_init (HilVariableClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->accept = accept;
    node_class->print = print;
}

static void
hil_variable_init (HilVariable *self)
{
    (void) self;
}

HilExpression *
hil_variable_get_initializer (HilVariable *variable)
{
    g_return_val_if_fail (HIL_IS_VARIABLE (variable), NULL);

    return g_object_ref (variable->initializer);
}

HilNode *
hil_variable_new (const gchar        *identifier,
                  HilValueType       *type,
                  HilExpression      *initializer,
                  HilSourceReference *source_reference)
{
    HilVariable *variable;

    g_return_val_if_fail (HIL_IS_VALUE_TYPE (type), NULL);
    g_return_val_if_fail (HIL_IS_EXPRESSION (initializer), NULL);

    variable = g_object_new (HIL_TYPE_VARIABLE,
                             "source-reference", source_reference,
                             "identifier", identifier,
                             "type", type,
                             NULL);

    variable->initializer = g_object_ref (initializer);

    return HIL_NODE (variable);
}
