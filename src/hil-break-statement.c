/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-break-statement.h"

struct _HilBreakStatement
{
    HilStatement parent_instance;
};

G_DEFINE_TYPE (HilBreakStatement, hil_break_statement, HIL_TYPE_STATEMENT)

static void
hil_break_statement_class_init (HilBreakStatementClass *klass)
{
    (void) klass;
}

static void
hil_break_statement_init (HilBreakStatement *self)
{
    (void) self;
}

HilNode *
hil_break_statement_new (void)
{
    return HIL_NODE (g_object_new (HIL_TYPE_BREAK_STATEMENT, NULL));
}
