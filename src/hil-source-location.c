/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-source-location.h"

struct _HilSourceLocation
{
    GObject parent_instance;

    const gchar *cursor;

    gsize line;
    gsize column;
};

G_DEFINE_TYPE (HilSourceLocation, hil_source_location, G_TYPE_OBJECT)

static void
hil_source_location_class_init (HilSourceLocationClass *klass)
{
    (void) klass;
}

static void
hil_source_location_init (HilSourceLocation *self)
{
    (void) self;
}

const gchar *
hil_source_location_get_cursor (HilSourceLocation *source_location)
{
    g_return_val_if_fail (HIL_IS_SOURCE_LOCATION (source_location), NULL);

    return source_location->cursor;
}

gsize
hil_source_location_get_line (HilSourceLocation *source_location)
{
    g_return_val_if_fail (HIL_IS_SOURCE_LOCATION (source_location), 0);

    return source_location->line;
}

gsize
hil_source_location_get_column (HilSourceLocation *source_location)
{
    g_return_val_if_fail (HIL_IS_SOURCE_LOCATION (source_location), 0);

    return source_location->column;
}

HilSourceLocation *
hil_source_location_new (const gchar *cursor,
                         gsize        line,
                         gsize        column)
{
    HilSourceLocation *source_location;

    g_return_val_if_fail (cursor != NULL, NULL);

    source_location = g_object_new (HIL_TYPE_SOURCE_LOCATION, NULL);

    source_location->cursor = cursor;
    source_location->line = line;
    source_location->column = column;

    return source_location;
}
