/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "hil-literal.h"

#define HIL_TYPE_STRING_LITERAL (hil_string_literal_get_type ())

G_DECLARE_FINAL_TYPE (HilStringLiteral, hil_string_literal,
                      HIL, STRING_LITERAL,
                      HilLiteral)

struct _HilSourceReference;

HilNode *hil_string_literal_new (const gchar                *value,
                                 struct _HilSourceReference *source_reference);
