/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>
#include <glib-object.h>

#define HIL_TYPE_SOURCE_REFERENCE (hil_source_reference_get_type ())

G_DECLARE_FINAL_TYPE (HilSourceReference, hil_source_reference,
                      HIL, SOURCE_REFERENCE,
                      GObject)

struct _HilSourceFile;
struct _HilSourceLocation;

struct _HilSourceLocation *hil_source_reference_get_end_location   (HilSourceReference        *source_reference);
struct _HilSourceFile     *hil_source_reference_get_source_file    (HilSourceReference        *source_reference);
struct _HilSourceLocation *hil_source_reference_get_start_location (HilSourceReference        *source_reference);

gchar                     *hil_source_reference_to_string          (HilSourceReference        *source_reference);

HilSourceReference        *hil_source_reference_new                (struct _HilSourceFile     *source_file,
                                                                    struct _HilSourceLocation *start_location,
                                                                    struct _HilSourceLocation *end_location);
