/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-while-statement.h"

struct _HilWhileStatement
{
    HilConditionalStatement parent_instance;

    HilStatement *body;
};

G_DEFINE_TYPE (HilWhileStatement, hil_while_statement,
               HIL_TYPE_CONDITIONAL_STATEMENT)

static void
finalize (GObject *object)
{
    HilWhileStatement *while_statement;

    while_statement = HIL_WHILE_STATEMENT (object);

    g_clear_object (&while_statement->body);

    G_OBJECT_CLASS (hil_while_statement_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilConditionalStatement *conditional_statement;
    g_autoptr (HilNode) condition = NULL;
    HilWhileStatement *while_statement;
    HilNode *body;
    gsize current_depth;

    conditional_statement = HIL_CONDITIONAL_STATEMENT (node);
    condition = hil_conditional_statement_get_condition (conditional_statement);
    while_statement = HIL_WHILE_STATEMENT (node);
    body = HIL_NODE (while_statement->body);

    HIL_NODE_CLASS (hil_while_statement_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Condition:\n");

    current_depth = *depth + 1;

    HIL_NODE_GET_CLASS (HIL_NODE (condition))->print (HIL_NODE (condition), &current_depth);

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Body:\n");

    current_depth = *depth + 1;

    HIL_NODE_GET_CLASS (body)->print (body, &current_depth);
}

static void
hil_while_statement_class_init (HilWhileStatementClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_while_statement_init (HilWhileStatement *self)
{
    (void) self;
}

HilNode *
hil_while_statement_new (HilExpression *condition,
                         HilStatement *body)
{
    HilWhileStatement *while_statement;

    g_return_val_if_fail (HIL_IS_STATEMENT (body), NULL);

    while_statement = g_object_new (HIL_TYPE_WHILE_STATEMENT,
                                    "condition", condition,
                                    NULL);

    while_statement->body = g_object_ref (body);

    return HIL_NODE (while_statement);
}
