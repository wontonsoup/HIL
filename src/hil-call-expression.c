/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-call-expression.h"

#include "hil-source-reference.h"

struct _HilCallExpression
{
    HilExpression parent_instance;

    gchar *identifier;
    GList *arguments;
};

G_DEFINE_TYPE (HilCallExpression, hil_call_expression, HIL_TYPE_EXPRESSION)

static void
finalize (GObject *object)
{
    HilCallExpression *call_expression;

    call_expression = HIL_CALL_EXPRESSION (object);

    g_clear_pointer (&call_expression->identifier, g_free);
    g_list_free_full (call_expression->arguments, g_object_unref);

    G_OBJECT_CLASS (hil_call_expression_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilCallExpression *call_expression;

    call_expression = HIL_CALL_EXPRESSION (node);

    HIL_NODE_CLASS (hil_call_expression_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Identifer: %s\n", call_expression->identifier);

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Arguments:\n");

    (*depth)++;

    for (GList *i = call_expression->arguments; i != NULL; i = i->next)
    {
        HilNode *argument_node;
        gsize current_depth;

        argument_node = HIL_NODE (i->data);
        current_depth = *depth;

        HIL_NODE_GET_CLASS (argument_node)->print (argument_node, &current_depth);
    }
}

static void
hil_call_expression_class_init (HilCallExpressionClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_call_expression_init (HilCallExpression *self)
{
    (void) self;
}

HilNode *
hil_call_expression_new (const gchar        *identifier,
                         GList              *arguments,
                         HilSourceReference *source_reference)
{
    HilCallExpression *call_expression;

    call_expression = g_object_new (HIL_TYPE_CALL_EXPRESSION,
                                    "source-reference", source_reference,
                                    NULL);

    call_expression->identifier = g_strdup (identifier);
    call_expression->arguments = arguments;

    return HIL_NODE (call_expression);
}
