/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-variable-expression.h"

#include "hil-source-reference.h"

struct _HilVariableExpression
{
    HilExpression parent_instance;

    gchar *identifier;
};

G_DEFINE_TYPE (HilVariableExpression, hil_variable_expression, HIL_TYPE_EXPRESSION)

static void
finalize (GObject *object)
{
    HilVariableExpression *variable_expression;

    variable_expression = HIL_VARIABLE_EXPRESSION (object);

    g_clear_pointer (&variable_expression->identifier, g_free);

    G_OBJECT_CLASS (hil_variable_expression_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilVariableExpression *variable_expression;

    variable_expression = HIL_VARIABLE_EXPRESSION (node);

    HIL_NODE_CLASS (hil_variable_expression_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Identifer: %s\n", variable_expression->identifier);
}

static void
hil_variable_expression_class_init (HilVariableExpressionClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_variable_expression_init (HilVariableExpression *self)
{
    (void) self;
}

HilNode *
hil_variable_expression_new (const gchar        *identifier,
                             HilSourceReference *source_reference)
{
    HilVariableExpression *variable_expression;

    variable_expression = g_object_new (HIL_TYPE_VARIABLE_EXPRESSION,
                                        "source-reference", source_reference,
                                        NULL);

    variable_expression->identifier = g_strdup (identifier);

    return HIL_NODE (variable_expression);
}
