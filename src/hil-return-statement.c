/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-return-statement.h"

#include "hil-expression.h"

struct _HilReturnStatement
{
    HilStatement parent_instance;

    HilExpression *expression;
};

G_DEFINE_TYPE (HilReturnStatement, hil_return_statement, HIL_TYPE_STATEMENT)

static void
finalize (GObject *object)
{
    HilReturnStatement *return_statement;

    return_statement = HIL_RETURN_STATEMENT (object);

    g_clear_object (&return_statement->expression);

    G_OBJECT_CLASS (hil_return_statement_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilReturnStatement *return_statement;
    HilNode *expression;

    return_statement = HIL_RETURN_STATEMENT (node);
    expression = HIL_NODE (return_statement->expression);

    HIL_NODE_CLASS (hil_return_statement_parent_class)->print (node, depth);

    (*depth)++;

    HIL_NODE_GET_CLASS (expression)->print (expression, depth);
}

static void
hil_return_statement_class_init (HilReturnStatementClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_return_statement_init (HilReturnStatement *self)
{
    (void) self;
}

HilNode *
hil_return_statement_new (HilExpression *expression)
{
    HilReturnStatement *return_statement;

    g_return_val_if_fail (HIL_IS_EXPRESSION (expression), NULL);

    return_statement = g_object_new (HIL_TYPE_RETURN_STATEMENT, NULL);

    return_statement->expression = g_object_ref (expression);

    return HIL_NODE (return_statement);
}
