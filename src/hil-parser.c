/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-parser.h"

#include "hil-source-file.h"
#include "hil-source-location.h"
#include "hil-source-reference.h"
#include "hil-token.h"

#include "hil-assignment-statement.h"
#include "hil-binary-expression.h"
#include "hil-boolean-literal.h"
#include "hil-boolean-type.h"
#include "hil-break-statement.h"
#include "hil-call-expression.h"
#include "hil-compound-statement.h"
#include "hil-continue-statement.h"
#include "hil-expression-statement.h"
#include "hil-function.h"
#include "hil-if-statement.h"
#include "hil-integer-literal.h"
#include "hil-integer-type.h"
#include "hil-real-literal.h"
#include "hil-real-type.h"
#include "hil-return-statement.h"
#include "hil-string-literal.h"
#include "hil-string-type.h"
#include "hil-subroutine-parameter.h"
#include "hil-unary-expression.h"
#include "hil-variable.h"
#include "hil-variable-expression.h"
#include "hil-void-type.h"
#include "hil-while-statement.h"

#include <stdarg.h>

#define ERROR(parser, format, ...) \
    {\
        g_autofree gchar *filename = NULL;\
        g_autoptr (HilSourceReference) source_reference = NULL;\
        g_autoptr (HilSourceLocation) start_location = NULL;\
        \
        filename = hil_source_file_get_name (parser->source_file);\
        source_reference = hil_token_get_source_reference (parser->tokens->data);\
        start_location = hil_source_reference_get_start_location (source_reference);\
        \
        g_printerr ("%s:%zu:%zu: error: " format,\
                    filename,\
                    hil_source_location_get_line (start_location),\
                    hil_source_location_get_column (start_location),\
                    __VA_ARGS__);\
    }

struct _HilParser
{
    HilVisitor parent_instance;

    HilSourceFile *source_file;
    GList *tokens;
};

G_DEFINE_TYPE (HilParser, hil_parser, HIL_TYPE_VISITOR)

static HilNode *parse_expression (HilParser *parser);
static HilNode *parse_statement  (HilParser *parser);

static void
finalize (GObject *object)
{
    HilParser *parser;

    parser = HIL_PARSER (object);

    g_clear_object (&parser->source_file);
    g_list_free_full (parser->tokens, g_object_unref);

    G_OBJECT_CLASS (hil_parser_parent_class)->finalize (object);
}

static HilToken *
current (HilParser *parser)
{
    if (parser->tokens == NULL)
    {
        return NULL;
    }

    return parser->tokens->data;
}

static void
next (HilParser *parser)
{
    if (parser->tokens != NULL)
    {
        parser->tokens = parser->tokens->next;
    }
}

static gboolean
accept (HilParser     *parser,
        HilTokenType   token_type,
        gchar        **value)
{
    HilToken *token;

    token = current (parser);
    if (token == NULL)
    {
        return FALSE;
    }

    if (hil_token_get_token_type (token) == token_type)
    {
        if (value != NULL)
        {
            *value = hil_token_get_value (token);
        }

        next (parser);

        return TRUE;
    }

    return FALSE;
}

static gboolean
expect (HilParser     *parser,
        HilTokenType   token_type,
        gchar        **value)
{
    if (!accept (parser, token_type, value))
    {
        ERROR (parser, "%s expected\n", hil_token_type_to_string (token_type));
    }

    return TRUE;
}

static gboolean
peek (HilParser    *parser,
      HilTokenType  token_type,
      ...)
{
    va_list ap;
    GList *tokens;

    tokens = parser->tokens;
    if (tokens == NULL)
    {
        return FALSE;
    }

    if (hil_token_get_token_type (tokens->data) != token_type)
    {
        return FALSE;
    }

    va_start (ap, token_type);

    for (gint i = va_arg (ap, gint); i != -1; i = va_arg (ap, gint))
    {
        tokens = tokens->next;

        if (tokens != NULL)
        {
            if (hil_token_get_token_type (tokens->data) == i)
            {
                continue;
            }
        }

        va_end (ap);

        return FALSE;
    }

    va_end (ap);

    return TRUE;
}

static HilNode *
parse_primary_expression (HilParser *parser)
{
    g_autoptr (HilSourceReference) start_reference = NULL;
    g_autoptr (HilSourceLocation) start_location = NULL;
    g_autofree gchar *value = NULL;
    HilTokenType token_type;
    g_autoptr (HilSourceReference) end_reference = NULL;
    g_autoptr (HilSourceLocation) end_location = NULL;
    g_autoptr (HilSourceReference) source_reference = NULL;
    HilNode *primary_expression;

    if (accept (parser, HIL_TOKEN_TYPE_PARENTHESIS_OPENING, NULL))
    {
        HilNode *expression;

        expression = parse_expression (parser);

        if (!expect (parser, HIL_TOKEN_TYPE_PARENTHESIS_CLOSING, NULL))
        {
            return NULL;
        }

        return expression;
    }

    start_reference = hil_token_get_source_reference (current (parser));
    start_location = hil_source_reference_get_start_location (start_reference);
    value = hil_token_get_value (current (parser));
    token_type = hil_token_get_token_type (current (parser));

    if (peek (parser, HIL_TOKEN_TYPE_IDENTIFIER, -1))
    {
        next (parser);

        if (accept (parser, HIL_TOKEN_TYPE_PARENTHESIS_OPENING, NULL))
        {
            GList *arguments = NULL;

            do
            {
                HilNode *argument;

                argument = parse_expression (parser);
                if (argument == NULL)
                {
                    g_list_free_full (arguments, g_object_unref);

                    return NULL;
                }
                arguments = g_list_prepend (arguments, argument);
            } while (accept (parser, HIL_TOKEN_TYPE_COMMA, NULL));

            if (!expect (parser, HIL_TOKEN_TYPE_PARENTHESIS_CLOSING, NULL))
            {
                g_list_free_full (arguments, g_object_unref);

                return NULL;
            }

            end_reference = hil_token_get_source_reference (current (parser));
            end_location = hil_source_reference_get_end_location (end_reference);
            source_reference = hil_source_reference_new (parser->source_file,
                                                         start_location, end_location);

            return hil_call_expression_new (value, arguments, source_reference);
        }
        else
        {
            end_reference = hil_token_get_source_reference (current (parser));
            end_location = hil_source_reference_get_end_location (end_reference);
            source_reference = hil_source_reference_new (parser->source_file,
                                                         start_location, end_location);

            return hil_variable_expression_new (value, source_reference);
        }
    }
    else
    {
        end_reference = hil_token_get_source_reference (current (parser));
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);

        next (parser);

        if (token_type == HIL_TOKEN_TYPE_LITERAL_BOOLEAN)
        {
            primary_expression = hil_boolean_literal_new (value, source_reference);
        }
        else if (token_type == HIL_TOKEN_TYPE_LITERAL_INTEGER)
        {
            primary_expression = hil_integer_literal_new (value, source_reference);
        }
        else if (token_type == HIL_TOKEN_TYPE_LITERAL_REAL)
        {
            primary_expression = hil_real_literal_new (value, source_reference);
        }
        else if (token_type == HIL_TOKEN_TYPE_LITERAL_STRING)
        {
            primary_expression = hil_string_literal_new (value, source_reference);
        }
    }

    return primary_expression;
}

static HilNode *
parse_unary_expression (HilParser *parser)
{
    g_autoptr (HilSourceReference) start_reference = NULL;
    g_autoptr (HilSourceLocation) start_location = NULL;
    g_autoptr (HilSourceReference) end_reference = NULL;
    g_autoptr (HilSourceLocation) end_location = NULL;
    g_autoptr (HilSourceReference) source_reference = NULL;

    start_reference = hil_token_get_source_reference (current (parser));
    start_location = hil_source_reference_get_start_location (start_reference);

    if (accept (parser, HIL_TOKEN_TYPE_MINUS, NULL))
    {
        g_autoptr (HilNode) primary_expression = NULL;

        primary_expression = parse_primary_expression (parser);
        end_reference = hil_token_get_source_reference (current (parser));
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);

        return hil_unary_expression_new (HIL_UNARY_OPERATOR_ARITHMETICAL_NEGATION,
                                         HIL_EXPRESSION (primary_expression),
                                         source_reference);
    }
    else if (accept (parser, HIL_TOKEN_TYPE_KEYWORD_NOT, NULL))
    {
        g_autoptr (HilNode) primary_expression = NULL;

        primary_expression = parse_primary_expression (parser);
        end_reference = hil_token_get_source_reference (current (parser));
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);

        return hil_unary_expression_new (HIL_UNARY_OPERATOR_LOGICAL_NEGATION,
                                         HIL_EXPRESSION (primary_expression),
                                         source_reference);
    }

    return parse_primary_expression (parser);
}

static HilNode *
parse_multiplicative_expression (HilParser *parser)
{
    HilNode *unary_expression;

    unary_expression = parse_unary_expression (parser);
    if (unary_expression == NULL)
    {
        return NULL;
    }

    while (peek (parser, HIL_TOKEN_TYPE_ASTERISK, -1) ||
           peek (parser, HIL_TOKEN_TYPE_SLASH, -1))
    {
        HilTokenType token_type = HIL_TOKEN_TYPE_UNKNOWN;
        HilBinaryOperator binary_operator;
        g_autoptr (HilSourceReference) start_reference = NULL;
        g_autoptr (HilSourceLocation) start_location = NULL;
        g_autoptr (HilNode) rhs_unary_expression = NULL;
        g_autoptr (HilSourceReference) end_reference = NULL;
        g_autoptr (HilSourceLocation) end_location = NULL;
        g_autoptr (HilSourceReference) source_reference = NULL;
        HilNode *binary_expression;

        token_type = hil_token_get_token_type (current (parser));
        if (token_type == HIL_TOKEN_TYPE_ASTERISK)
        {
            binary_operator = HIL_BINARY_OPERATOR_MULTIPLICATION;
        }
        else if (token_type == HIL_TOKEN_TYPE_SLASH)
        {
            binary_operator = HIL_BINARY_OPERATOR_DIVISION;
        }
        else
        {
            return NULL;
        }

        next (parser);

        start_reference = hil_node_get_source_reference (unary_expression);
        start_location = hil_source_reference_get_start_location (start_reference);
        rhs_unary_expression = parse_unary_expression (parser);
        end_reference = hil_node_get_source_reference (rhs_unary_expression);
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);
        binary_expression = hil_binary_expression_new (binary_operator,
                                                       HIL_EXPRESSION (unary_expression),
                                                       HIL_EXPRESSION (rhs_unary_expression),
                                                       source_reference);

        g_object_unref (unary_expression);

        unary_expression = binary_expression;
    }

    return unary_expression;
}

static HilNode *
parse_additive_expression (HilParser *parser)
{
    HilNode *multiplicative_expression;

    multiplicative_expression = parse_multiplicative_expression (parser);

    while (peek (parser, HIL_TOKEN_TYPE_PLUS, -1) ||
           peek (parser, HIL_TOKEN_TYPE_MINUS, -1))
    {
        HilTokenType token_type = HIL_TOKEN_TYPE_UNKNOWN;
        HilBinaryOperator binary_operator;
        g_autoptr (HilSourceReference) start_reference = NULL;
        g_autoptr (HilSourceLocation) start_location = NULL;
        g_autoptr (HilNode) rhs_multiplicative_expression = NULL;
        g_autoptr (HilSourceReference) end_reference = NULL;
        g_autoptr (HilSourceLocation) end_location = NULL;
        g_autoptr (HilSourceReference) source_reference = NULL;
        HilNode *binary_expression;

        token_type = hil_token_get_token_type (current (parser));
        if (token_type == HIL_TOKEN_TYPE_PLUS)
        {
            binary_operator = HIL_BINARY_OPERATOR_ADDITION;
        }
        else if (token_type == HIL_TOKEN_TYPE_MINUS)
        {
            binary_operator = HIL_BINARY_OPERATOR_SUBTRACTION;
        }
        else
        {
            return NULL;
        }

        next (parser);

        start_reference = hil_node_get_source_reference (multiplicative_expression);
        start_location = hil_source_reference_get_start_location (start_reference);
        rhs_multiplicative_expression = parse_multiplicative_expression (parser);
        end_reference = hil_node_get_source_reference (rhs_multiplicative_expression);
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);
        binary_expression =
            hil_binary_expression_new (binary_operator,
                                       HIL_EXPRESSION (multiplicative_expression),
                                       HIL_EXPRESSION (rhs_multiplicative_expression),
                                       source_reference);

        g_object_unref (multiplicative_expression);

        multiplicative_expression = binary_expression;
    }

    return multiplicative_expression;
}

static HilNode *
parse_relational_expression (HilParser *parser)
{
    HilNode *additive_expression;

    additive_expression = parse_additive_expression (parser);

    while (peek (parser, HIL_TOKEN_TYPE_OPERATOR_LT, -1) ||
           peek (parser, HIL_TOKEN_TYPE_OPERATOR_GT, -1) ||
           peek (parser, HIL_TOKEN_TYPE_OPERATOR_LE, -1) ||
           peek (parser, HIL_TOKEN_TYPE_OPERATOR_GE, -1))
    {
        HilTokenType token_type;
        HilBinaryOperator binary_operator;
        g_autoptr (HilSourceReference) start_reference = NULL;
        g_autoptr (HilSourceLocation) start_location = NULL;
        g_autoptr (HilNode) rhs_additive_expression = NULL;
        g_autoptr (HilSourceReference) end_reference = NULL;
        g_autoptr (HilSourceLocation) end_location = NULL;
        g_autoptr (HilSourceReference) source_reference = NULL;
        HilNode *binary_expression;

        token_type = hil_token_get_token_type (current (parser));
        if (token_type == HIL_TOKEN_TYPE_OPERATOR_LT)
        {
            binary_operator = HIL_BINARY_OPERATOR_LESS_THAN;
        }
        else if (token_type == HIL_TOKEN_TYPE_OPERATOR_GT)
        {
            binary_operator = HIL_BINARY_OPERATOR_GREATER_THAN;
        }
        else if (token_type == HIL_TOKEN_TYPE_OPERATOR_LE)
        {
            binary_operator = HIL_BINARY_OPERATOR_LESS_THAN_OR_EQUAL;
        }
        else if (token_type == HIL_TOKEN_TYPE_OPERATOR_GE)
        {
            binary_operator = HIL_BINARY_OPERATOR_GREATER_THAN_OR_EQUAL;
        }
        else
        {
            return NULL;
        }

        next (parser);

        start_reference = hil_node_get_source_reference (additive_expression);
        start_location = hil_source_reference_get_start_location (start_reference);
        rhs_additive_expression = parse_additive_expression (parser);
        end_reference = hil_node_get_source_reference (rhs_additive_expression);
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);
        binary_expression =
            hil_binary_expression_new (binary_operator,
                                       HIL_EXPRESSION (additive_expression),
                                       HIL_EXPRESSION (rhs_additive_expression),
                                       source_reference);

        g_object_unref (additive_expression);

        additive_expression = binary_expression;
    }

    return additive_expression;
}

static HilNode *
parse_equality_expression (HilParser *parser)
{
    HilNode *relational_expression;

    relational_expression = parse_relational_expression (parser);

    while (peek (parser, HIL_TOKEN_TYPE_OPERATOR_EQ, -1) ||
           peek (parser, HIL_TOKEN_TYPE_OPERATOR_NEQ, -1))
    {
        HilTokenType token_type = HIL_TOKEN_TYPE_UNKNOWN;
        HilBinaryOperator binary_operator;
        g_autoptr (HilSourceReference) start_reference = NULL;
        g_autoptr (HilSourceLocation) start_location = NULL;
        g_autoptr (HilNode) rhs_relational_expression = NULL;
        g_autoptr (HilSourceReference) end_reference = NULL;
        g_autoptr (HilSourceLocation) end_location = NULL;
        g_autoptr (HilSourceReference) source_reference = NULL;
        HilNode *binary_expression;

        token_type = hil_token_get_token_type (current (parser));
        if (token_type == HIL_TOKEN_TYPE_OPERATOR_EQ)
        {
            binary_operator = HIL_BINARY_OPERATOR_EQUALITY;
        }
        else if (token_type == HIL_TOKEN_TYPE_OPERATOR_NEQ)
        {
            binary_operator = HIL_BINARY_OPERATOR_INEQUALITY;
        }
        else
        {
            return NULL;
        }

        next (parser);

        start_reference = hil_node_get_source_reference (relational_expression);
        start_location = hil_source_reference_get_start_location (start_reference);
        rhs_relational_expression = parse_relational_expression (parser);
        end_reference = hil_node_get_source_reference (rhs_relational_expression);
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);
        binary_expression =
            hil_binary_expression_new (binary_operator,
                                       HIL_EXPRESSION (relational_expression),
                                       HIL_EXPRESSION (rhs_relational_expression),
                                       source_reference);

        g_object_unref (relational_expression);

        relational_expression = binary_expression;
    }

    return relational_expression;
}

static HilNode *
parse_logical_conjunction_expression (HilParser *parser)
{
    HilNode *equality_expression;

    equality_expression = parse_equality_expression (parser);

    while (accept (parser, HIL_TOKEN_TYPE_KEYWORD_AND, NULL))
    {
        g_autoptr (HilSourceReference) start_reference = NULL;
        g_autoptr (HilSourceLocation) start_location = NULL;
        g_autoptr (HilNode) rhs_equality_expression = NULL;
        g_autoptr (HilSourceReference) end_reference = NULL;
        g_autoptr (HilSourceLocation) end_location = NULL;
        g_autoptr (HilSourceReference) source_reference = NULL;
        HilNode *binary_expression;

        start_reference = hil_node_get_source_reference (equality_expression);
        start_location = hil_source_reference_get_start_location (start_reference);
        rhs_equality_expression = parse_expression (parser);
        end_reference = hil_node_get_source_reference (rhs_equality_expression);
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);
        binary_expression =
            hil_binary_expression_new (HIL_BINARY_OPERATOR_LOGICAL_CONJUNCTION,
                                       HIL_EXPRESSION (equality_expression),
                                       HIL_EXPRESSION (rhs_equality_expression),
                                       source_reference);

        g_object_unref (equality_expression);

        equality_expression = binary_expression;
    }

    return equality_expression;
}

static HilNode *
parse_logical_disjunction_expression (HilParser *parser)
{
    HilNode *logical_conjunction;

    logical_conjunction = parse_logical_conjunction_expression (parser);

    while (accept (parser, HIL_TOKEN_TYPE_KEYWORD_OR, NULL))
    {
        g_autoptr (HilSourceReference) start_reference = NULL;
        g_autoptr (HilSourceLocation) start_location = NULL;
        g_autoptr (HilNode) rhs_logical_conjunction = NULL;
        g_autoptr (HilSourceReference) end_reference = NULL;
        g_autoptr (HilSourceLocation) end_location = NULL;
        g_autoptr (HilSourceReference) source_reference = NULL;
        HilNode *binary_expression;

        start_reference = hil_node_get_source_reference (logical_conjunction);
        start_location = hil_source_reference_get_start_location (start_reference);
        rhs_logical_conjunction = parse_logical_conjunction_expression (parser);
        end_reference = hil_node_get_source_reference (rhs_logical_conjunction);
        end_location = hil_source_reference_get_end_location (end_reference);
        source_reference = hil_source_reference_new (parser->source_file,
                                                     start_location, end_location);
        binary_expression =
            hil_binary_expression_new (HIL_BINARY_OPERATOR_LOGICAL_DISJUNCTION,
                                       HIL_EXPRESSION (logical_conjunction),
                                       HIL_EXPRESSION (rhs_logical_conjunction),
                                       source_reference);

        g_object_unref (logical_conjunction);

        logical_conjunction = binary_expression;
    }

    return logical_conjunction;
}

static HilNode *
parse_expression (HilParser *parser)
{
    return parse_logical_disjunction_expression (parser);
}

static HilNode *
parse_type (HilParser *parser)
{
    HilToken *token;
    HilTokenType token_type;
    g_autofree gchar *token_value = NULL;

    token = current (parser);
    token_type = hil_token_get_token_type (token);
    token_value = hil_token_get_value (token);

    next (parser);

    if (token_type == HIL_TOKEN_TYPE_BUILT_IN_TYPE_BOOLEAN)
    {
        return hil_boolean_type_new ();
    }
    if (token_type == HIL_TOKEN_TYPE_BUILT_IN_TYPE_DOUBLE)
    {
        return hil_real_type_new ();
    }
    if (token_type == HIL_TOKEN_TYPE_BUILT_IN_TYPE_INT)
    {
        return hil_integer_type_new ();
    }
    if (token_type == HIL_TOKEN_TYPE_BUILT_IN_TYPE_STRING)
    {
        return hil_string_type_new ();
    }

    ERROR (parser, "Unrecognized type %s\n", token_value);

    return NULL;
}

static HilNode *
parse_variable_declaration (HilParser *parser)
{
    g_autoptr (HilSourceReference) start_reference = NULL;
    g_autoptr (HilSourceLocation) start_location = NULL;
    g_autofree gchar *identifier = NULL;
    g_autoptr (HilNode) type = NULL;
    g_autoptr (HilNode) expression = NULL;
    g_autoptr (HilSourceReference) end_reference = NULL;
    g_autoptr (HilSourceLocation) end_location = NULL;
    g_autoptr (HilSourceReference) source_reference = NULL;
    g_autoptr (HilNode) variable_declaration = NULL;

    start_reference = hil_token_get_source_reference (current (parser));
    start_location = hil_source_reference_get_start_location (start_reference);

    if (!expect (parser, HIL_TOKEN_TYPE_IDENTIFIER, &identifier) ||
        !expect (parser, HIL_TOKEN_TYPE_COLON, NULL))
    {
        return NULL;
    }
    type = parse_type (parser);
    if (type == NULL)
    {
        return NULL;
    }

    if (!expect (parser, HIL_TOKEN_TYPE_ASSIGN, NULL))
    {
        return NULL;
    }

    expression = parse_expression (parser);
    end_reference = hil_token_get_source_reference (current (parser));
    end_location = hil_source_reference_get_end_location (end_reference);
    source_reference = hil_source_reference_new (parser->source_file,
                                                 start_location, end_location);
    variable_declaration = hil_variable_new (identifier, HIL_VALUE_TYPE (type),
                                             HIL_EXPRESSION (expression),
                                             source_reference);

    if (!expect (parser, HIL_TOKEN_TYPE_SEMICOLON, NULL))
    {
        return NULL;
    }

    return g_object_ref (variable_declaration);
}

static HilNode *
parse_compound_statement (HilParser *parser)
{
    GList *statements = NULL;

    if (!expect (parser, HIL_TOKEN_TYPE_BRACE_OPENING, NULL))
    {
        return NULL;
    }

    while (!accept (parser, HIL_TOKEN_TYPE_BRACE_CLOSING, NULL))
    {
        if (peek (parser, HIL_TOKEN_TYPE_IDENTIFIER, HIL_TOKEN_TYPE_COLON, -1))
        {
            statements = g_list_prepend (statements,
                                         parse_variable_declaration (parser));
        }
        else
        {
            statements = g_list_prepend (statements, parse_statement (parser));
        }
    }

    return hil_compound_statement_new (g_list_reverse (statements));
}

static HilNode *
parse_subroutine_definition (HilParser *parser)
{
    g_autofree gchar *identifier = NULL;
    GList *parameters = NULL;
    g_autoptr (HilNode) return_type = NULL;
    g_autoptr (HilNode) body = NULL;

    if (!expect (parser, HIL_TOKEN_TYPE_IDENTIFIER, &identifier) ||
        !expect (parser, HIL_TOKEN_TYPE_PARENTHESIS_OPENING, NULL))
    {
        return NULL;
    }

    while (peek (parser, HIL_TOKEN_TYPE_IDENTIFIER, -1))
    {
        HilToken *token;
        g_autofree gchar *parameter_name = NULL;
        g_autofree gchar *parameter_type = NULL;
        HilSubroutineParameter *parameter;

        token = current (parser);
        if (token == NULL)
        {
            return NULL;
        }
        parameter_name = hil_token_get_value (token);

        next (parser);

        if (!expect (parser, HIL_TOKEN_TYPE_COLON, NULL))
        {
            g_list_free_full (parameters, g_object_unref);

            return NULL;
        }

        token = current (parser);
        if (token == NULL)
        {
            return NULL;
        }
        parameter_type = hil_token_get_value (token);
        parameter = hil_subroutine_parameter_new (parameter_name, parameter_type);

        parameters = g_list_prepend (parameters, parameter);

        next (parser);

        if (!accept (parser, HIL_TOKEN_TYPE_COMMA, NULL))
        {
            break;
        }
    }

    if (!expect (parser, HIL_TOKEN_TYPE_PARENTHESIS_CLOSING, NULL))
    {
        return NULL;
    }

    if (accept (parser, HIL_TOKEN_TYPE_COLON, NULL))
    {
        return_type = parse_type (parser);
        if (return_type == NULL)
        {
            return NULL;
        }
    }
    else
    {
        return_type = hil_void_type_new ();
    }

    if (!expect (parser, HIL_TOKEN_TYPE_ASSIGN, NULL))
    {
        g_list_free_full (parameters, g_object_unref);

        return NULL;
    }

    body = parse_compound_statement (parser);

    return hil_function_new (identifier,
                             g_list_reverse (parameters),
                             HIL_TYPE (return_type),
                             HIL_COMPOUND_STATEMENT (body));
}

static HilNode *
parse_definition (HilParser *parser)
{
    if (peek (parser, HIL_TOKEN_TYPE_IDENTIFIER,
                      HIL_TOKEN_TYPE_COLON, -1))
    {
        return parse_variable_declaration (parser);
    }
    else if (peek (parser, HIL_TOKEN_TYPE_IDENTIFIER,
                           HIL_TOKEN_TYPE_PARENTHESIS_OPENING, -1))
    {
        return parse_subroutine_definition (parser);
    }

    return NULL;
}

static HilNode *
parse_statement (HilParser *parser)
{
    HilToken *token;
    g_autoptr (HilNode) expression = NULL;

    token = current (parser);
    if (token == NULL)
    {
        return NULL;
    }

    switch (hil_token_get_token_type (token))
    {
        case HIL_TOKEN_TYPE_BRACE_OPENING:
        {
            return parse_compound_statement (parser);
        }
        break;

        case HIL_TOKEN_TYPE_KEYWORD_BREAK:
        {
            next (parser);

            if (!expect (parser, HIL_TOKEN_TYPE_SEMICOLON, NULL))
            {
                return NULL;
            }

            return hil_break_statement_new ();
        }
        break;

        case HIL_TOKEN_TYPE_KEYWORD_CONTINUE:
        {
            next (parser);

            if (!expect (parser, HIL_TOKEN_TYPE_SEMICOLON, NULL))
            {
                return NULL;
            }

            return hil_continue_statement_new ();
        }
        break;

        case HIL_TOKEN_TYPE_IDENTIFIER:
        {
            HilToken *token;
            g_autofree gchar *identifier = NULL;
            g_autoptr (HilNode) expression = NULL;

            token = current (parser);
            if (token == NULL)
            {
                return NULL;
            }

            identifier = hil_token_get_value (token);

            if (!peek (parser,
                       HIL_TOKEN_TYPE_IDENTIFIER, HIL_TOKEN_TYPE_ASSIGN, -1))
            {
                break;
            }

            next (parser);
            next (parser);

            expression = parse_expression (parser);

            if (!expect (parser, HIL_TOKEN_TYPE_SEMICOLON, NULL))
            {
                return NULL;
            }

            return hil_assignment_statement_new (identifier,
                                                 HIL_EXPRESSION (expression));
        }
        break;

        case HIL_TOKEN_TYPE_KEYWORD_IF:
        {
            g_autoptr (HilNode) body = NULL;
            g_autoptr (HilNode) else_substatement = NULL;

            next (parser);

            expression = parse_expression (parser);
            body = parse_statement (parser);

            if (accept (parser, HIL_TOKEN_TYPE_KEYWORD_ELSE, NULL))
            {
                else_substatement = parse_statement (parser);
            }

            return hil_if_statement_new (HIL_EXPRESSION (expression),
                                         HIL_STATEMENT (body),
                                         HIL_STATEMENT (else_substatement));
        }
        break;

        case HIL_TOKEN_TYPE_KEYWORD_RETURN:
        {
            next (parser);

            expression = parse_expression (parser);

            if (!expect (parser, HIL_TOKEN_TYPE_SEMICOLON, NULL))
            {
                return NULL;
            }

            return hil_return_statement_new (HIL_EXPRESSION (expression));
        }
        break;

        case HIL_TOKEN_TYPE_KEYWORD_WHILE:
        {
            g_autoptr (HilNode) condition = NULL;
            g_autoptr (HilNode) body = NULL;

            next (parser);

            condition = parse_expression (parser);
            body = parse_statement (parser);

            return hil_while_statement_new (HIL_EXPRESSION (condition), HIL_STATEMENT (body));
        }
        break;

        default:
        {
        }
        break;
    }

    expression = parse_expression (parser);

    if (!expect (parser, HIL_TOKEN_TYPE_SEMICOLON, NULL))
    {
        return NULL;
    }

    return hil_expression_statement_new (HIL_EXPRESSION (expression));
}

static void
parse_program (HilParser *parser)
{
    HilNode *node;

    while (!peek (parser, HIL_TOKEN_TYPE_EOF, -1))
    {
        if (peek (parser, HIL_TOKEN_TYPE_IDENTIFIER,
                          HIL_TOKEN_TYPE_COLON, -1) ||
            peek (parser, HIL_TOKEN_TYPE_IDENTIFIER,
                          HIL_TOKEN_TYPE_PARENTHESIS_OPENING,
                          HIL_TOKEN_TYPE_PARENTHESIS_CLOSING,
                          HIL_TOKEN_TYPE_ASSIGN, -1) ||
            peek (parser, HIL_TOKEN_TYPE_IDENTIFIER,
                          HIL_TOKEN_TYPE_PARENTHESIS_OPENING,
                          HIL_TOKEN_TYPE_PARENTHESIS_CLOSING,
                          HIL_TOKEN_TYPE_COLON, -1) ||
            peek (parser, HIL_TOKEN_TYPE_IDENTIFIER,
                          HIL_TOKEN_TYPE_PARENTHESIS_OPENING,
                          HIL_TOKEN_TYPE_IDENTIFIER,
                          HIL_TOKEN_TYPE_COLON, -1))
        {
            node = parse_definition (parser);
        }
        else
        {
            node = parse_statement (parser);
        }

        if (node == NULL)
        {
            break;
        }

        hil_source_file_add_node (parser->source_file, node);
    }
}

static void
visit_source_file (HilVisitor    *visitor,
                   HilSourceFile *source_file)
{
    HilParser *parser;

    parser = HIL_PARSER (visitor);

    parser->source_file = source_file;
    parser->tokens = hil_source_file_get_tokens (parser->source_file);

    parse_program (parser);
}

static void
hil_parser_class_init (HilParserClass *klass)
{
    GObjectClass *object_class;
    HilVisitorClass *visitor_class;

    object_class = G_OBJECT_CLASS (klass);
    visitor_class = HIL_VISITOR_CLASS (klass);

    object_class->finalize = finalize;

    visitor_class->visit_source_file = visit_source_file;
}

static void
hil_parser_init (HilParser *self)
{
    (void) self;
}

void
hil_parser_parse (HilParser     *parser,
                  HilSourceFile *source_file)
{
    g_return_if_fail (HIL_IS_PARSER (parser));
    g_return_if_fail (HIL_IS_SOURCE_FILE (source_file));

    parser->source_file = g_object_ref (source_file);

    hil_source_file_accept (source_file, HIL_VISITOR (parser));

    {
        GList *nodes;

        nodes = hil_source_file_get_nodes (source_file);
        if (nodes == NULL)
        {
            return;
        }

        for (GList *i = nodes; i != NULL; i = i->next)
        {
            hil_node_print (i->data);
        }
    }
}

HilParser *
hil_parser_new (void)
{
    return g_object_new (HIL_TYPE_PARSER, NULL);
}
