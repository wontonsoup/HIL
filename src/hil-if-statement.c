/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-if-statement.h"

struct _HilIfStatement
{
    HilConditionalStatement parent_instance;

    HilStatement *body;
    HilStatement *else_substatement;
};

G_DEFINE_TYPE (HilIfStatement, hil_if_statement,
               HIL_TYPE_CONDITIONAL_STATEMENT)

static void
finalize (GObject *object)
{
    HilIfStatement *if_statement;

    if_statement = HIL_IF_STATEMENT (object);

    g_clear_object (&if_statement->body);
    g_clear_object (&if_statement->else_substatement);

    G_OBJECT_CLASS (hil_if_statement_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilConditionalStatement *conditional_statement;
    g_autoptr (HilNode) condition = NULL;
    HilIfStatement *if_statement;
    HilNode *body;
    HilNode *else_substatement;
    gsize current_depth;

    conditional_statement = HIL_CONDITIONAL_STATEMENT (node);
    condition = hil_conditional_statement_get_condition (conditional_statement);
    if_statement = HIL_IF_STATEMENT (node);
    body = HIL_NODE (if_statement->body);
    else_substatement = HIL_NODE (if_statement->else_substatement);

    HIL_NODE_CLASS (hil_if_statement_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Condition:\n");

    current_depth = *depth + 1;

    HIL_NODE_GET_CLASS (HIL_NODE (condition))->print (HIL_NODE (condition), &current_depth);

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Body:\n");

    current_depth = *depth + 1;

    HIL_NODE_GET_CLASS (body)->print (body, &current_depth);

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Else substatement:\n");

    current_depth = *depth + 1;

    HIL_NODE_GET_CLASS (else_substatement)->print (else_substatement, &current_depth);
}

static void
hil_if_statement_class_init (HilIfStatementClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_if_statement_init (HilIfStatement *self)
{
    (void) self;
}

HilNode *
hil_if_statement_new (HilExpression *condition,
                      HilStatement  *body,
                      HilStatement  *else_substatement)
{
    HilIfStatement *if_statement;

    g_return_val_if_fail (HIL_IS_STATEMENT (body), NULL);
    if (else_substatement != NULL)
    {
        g_return_val_if_fail (HIL_IS_STATEMENT (else_substatement), NULL);
    }

    if_statement = g_object_new (HIL_TYPE_IF_STATEMENT,
                                 "condition", condition,
                                 NULL);

    if_statement->body = g_object_ref (body);
    if (else_substatement != NULL)
    {
        if_statement->else_substatement = g_object_ref (else_substatement);
    }

    return HIL_NODE (if_statement);
}
