/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-token.h"

#include "hil-source-location.h"
#include "hil-source-reference.h"

struct _HilToken
{
    GObject parent_instance;

    HilTokenType type;

    HilSourceReference *source_reference;
};

G_DEFINE_TYPE (HilToken, hil_token, G_TYPE_OBJECT)

static void
finalize (GObject *object)
{
    HilToken *token;

    token = HIL_TOKEN (object);

    token->type = HIL_TOKEN_TYPE_UNKNOWN;
    g_clear_object (&token->source_reference);

    G_OBJECT_CLASS (hil_token_parent_class)->finalize (object);
}

static void
hil_token_class_init (HilTokenClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = finalize;
}

static void
hil_token_init (HilToken *self)
{
    (void) self;
}

gboolean
hil_token_type_is_built_in_type (HilTokenType type)
{
    return (type >= HIL_TOKEN_TYPE_BUILT_IN_TYPE_BOOLEAN) &&
           (type <= HIL_TOKEN_TYPE_BUILT_IN_TYPE_STRING);
}

const gchar *
hil_token_type_to_string (HilTokenType type)
{
    switch (type)
    {
        case HIL_TOKEN_TYPE_ASTERISK:
        {
            return "*";
        }
        break;
        case HIL_TOKEN_TYPE_MINUS:
        {
            return "-";
        }
        break;
        case HIL_TOKEN_TYPE_PLUS:
        {
            return "+";
        }
        break;
        case HIL_TOKEN_TYPE_SLASH:
        {
            return "/";
        }
        break;

        case HIL_TOKEN_TYPE_ASSIGN:
        {
            return "=";
        }
        break;

        case HIL_TOKEN_TYPE_ASSIGN_ADD:
        {
            return "+=";
        }
        break;

        case HIL_TOKEN_TYPE_ASSIGN_DIV:
        {
            return "/=";
        }
        break;

        case HIL_TOKEN_TYPE_ASSIGN_MUL:
        {
            return "*=";
        }
        break;

        case HIL_TOKEN_TYPE_BRACE_CLOSING:
        {
            return "}";
        }
        break;

        case HIL_TOKEN_TYPE_BRACE_OPENING:
        {
            return "{";
        }
        break;

        case HIL_TOKEN_TYPE_BUILT_IN_TYPE_DOUBLE:
        {
            return "double";
        }
        break;

        case HIL_TOKEN_TYPE_BUILT_IN_TYPE_INT:
        {
            return "int";
        }
        break;

        case HIL_TOKEN_TYPE_BUILT_IN_TYPE_STRING:
        {
            return "string";
        }
        break;

        case HIL_TOKEN_TYPE_COLON:
        {
            return ":";
        }
        break;

        case HIL_TOKEN_TYPE_COMMA:
        {
            return ",";
        }
        break;

        case HIL_TOKEN_TYPE_IDENTIFIER:
        {
            return "identifier";
        }
        break;

        case HIL_TOKEN_TYPE_KEYWORD_BREAK:
        {
            return "break";
        }
        break;

        case HIL_TOKEN_TYPE_KEYWORD_CONTINUE:
        {
            return "continue";
        }
        break;

        case HIL_TOKEN_TYPE_KEYWORD_RETURN:
        {
            return "return";
        }
        break;

        case HIL_TOKEN_TYPE_PARENTHESIS_CLOSING:
        {
            return ")";
        }
        break;

        case HIL_TOKEN_TYPE_PARENTHESIS_OPENING:
        {
            return "(";
        }
        break;

        case HIL_TOKEN_TYPE_SEMICOLON:
        {
            return ";";
        }
        break;

        default:
        {
            return "unknown";
        }
        break;
    }

    return NULL;
}

HilTokenType
hil_token_get_token_type (HilToken *token)
{
    g_return_val_if_fail (HIL_IS_TOKEN (token), HIL_TOKEN_TYPE_UNKNOWN);

    return token->type;
}

HilSourceReference *
hil_token_get_source_reference (HilToken *token)
{
    g_return_val_if_fail (HIL_IS_TOKEN (token), NULL);

    return g_object_ref (token->source_reference);
}

gchar *
hil_token_get_value (HilToken *token)
{
    g_autoptr (HilSourceLocation) start_location = NULL;
    g_autoptr (HilSourceLocation) end_location = NULL;
    const gchar *cursor_start;
    const gchar *cursor_end;

    g_return_val_if_fail (HIL_IS_TOKEN (token), NULL);

    start_location = hil_source_reference_get_start_location (token->source_reference);
    end_location = hil_source_reference_get_end_location (token->source_reference);
    cursor_start = hil_source_location_get_cursor (start_location);
    cursor_end = hil_source_location_get_cursor (end_location);

    return g_strndup (cursor_start, cursor_end - cursor_start);
}

gchar *
hil_token_to_string (HilToken *token)
{
    g_autofree gchar *source_reference_string = NULL;
    g_autofree gchar *value = NULL;

    g_return_val_if_fail (HIL_IS_TOKEN (token), NULL);

    source_reference_string = hil_source_reference_to_string (token->source_reference);
    value = hil_token_get_value (token);

    return g_strdup_printf ("%s:\t%s\t(%2d)",
                            source_reference_string,
                            value,
                            token->type);
}

HilToken *
hil_token_new (HilTokenType        type,
               HilSourceReference *source_reference)
{
    HilToken *token;

    token = g_object_new (HIL_TYPE_TOKEN, NULL);

    token->type = type;
    token->source_reference = g_object_ref (source_reference);

    return token;
}
