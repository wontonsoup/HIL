/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-unary-expression.h"

#include "hil-source-reference.h"

struct _HilUnaryExpression
{
    HilExpression parent_instance;

    HilUnaryOperator unary_operator;

    HilExpression *expression;
};

G_DEFINE_TYPE (HilUnaryExpression, hil_unary_expression, HIL_TYPE_EXPRESSION)

static void
finalize (GObject *object)
{
    HilUnaryExpression *unary_expression;

    unary_expression = HIL_UNARY_EXPRESSION (object);

    g_clear_object (&unary_expression->expression);

    G_OBJECT_CLASS (hil_unary_expression_parent_class)->finalize (object);
}

static const gchar *
operator_to_string (HilUnaryOperator operator)
{
    switch (operator)
    {
        case HIL_UNARY_OPERATOR_ARITHMETICAL_NEGATION:
        {
            return "arithmetical negation";
        }
        break;

        case HIL_UNARY_OPERATOR_LOGICAL_NEGATION:
        {
            return "logical negation";
        }
        break;
    }

    return "unknown";
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilUnaryExpression *unary_expression;
    HilNode *expression;

    unary_expression = HIL_UNARY_EXPRESSION (node);
    expression = HIL_NODE (unary_expression->expression);

    HIL_NODE_CLASS (hil_unary_expression_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Operator: %s\n",
             operator_to_string (unary_expression->unary_operator));

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Expression:\n");

    (*depth)++;

    HIL_NODE_GET_CLASS (expression)->print (expression, depth);
}

static void
hil_unary_expression_class_init (HilUnaryExpressionClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_unary_expression_init (HilUnaryExpression *self)
{
    (void) self;
}

HilNode *
hil_unary_expression_new (HilUnaryOperator    unary_operator,
                          HilExpression      *expression,
                          HilSourceReference *source_reference)
{
    HilUnaryExpression *unary_expression;

    g_return_val_if_fail (HIL_IS_EXPRESSION (expression), NULL);

    unary_expression = g_object_new (HIL_TYPE_UNARY_EXPRESSION,
                                     "source-reference", source_reference,
                                     NULL);

    unary_expression->unary_operator = unary_operator;
    unary_expression->expression = g_object_ref (expression);

    return HIL_NODE (unary_expression);
}
