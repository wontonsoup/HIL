/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#define HIL_TYPE_SUBROUTINE_PARAMETER (hil_subroutine_parameter_get_type ())

G_DECLARE_FINAL_TYPE (HilSubroutineParameter, hil_subroutine_parameter,
                      HIL, SUBROUTINE_PARAMETER, GObject)

gchar *hil_subroutine_parameter_get_identifier     (HilSubroutineParameter *parameter);
gchar *hil_subroutine_parameter_get_parameter_type (HilSubroutineParameter *parameter);

HilSubroutineParameter *hil_subroutine_parameter_new (const gchar *identifer,
                                                      const gchar *type);
