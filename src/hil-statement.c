/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-statement.h"

G_DEFINE_ABSTRACT_TYPE (HilStatement, hil_statement, HIL_TYPE_NODE)

static void
print (HilNode *node,
       gsize   *depth)
{
    HIL_NODE_CLASS (hil_statement_parent_class)->print (node, depth);
}

static void
hil_statement_class_init (HilStatementClass *klass)
{
    HilNodeClass *node_class;

    node_class = HIL_NODE_CLASS (klass);

    node_class->print = print;
}

static void
hil_statement_init (HilStatement *self)
{
    (void) self;
}
