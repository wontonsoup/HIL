/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-assignment-statement.h"

#include "hil-expression.h"

struct _HilAssignmentStatement
{
    HilStatement parent_instance;

    gchar *identifier;
    HilExpression *value;
};

G_DEFINE_TYPE (HilAssignmentStatement, hil_assignment_statement, HIL_TYPE_STATEMENT)

static void
finalize (GObject *object)
{
    HilAssignmentStatement *assignment_statement;

    assignment_statement = HIL_ASSIGNMENT_STATEMENT (object);

    g_clear_pointer (&assignment_statement->identifier, g_free);
    g_clear_object (&assignment_statement->value);

    G_OBJECT_CLASS (hil_assignment_statement_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilAssignmentStatement *assignment_statement;
    HilNode *value;

    assignment_statement = HIL_ASSIGNMENT_STATEMENT (node);
    value = HIL_NODE (assignment_statement->value);

    HIL_NODE_CLASS (hil_assignment_statement_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Identifier: %s\n", assignment_statement->identifier);

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Value:\n");

    (*depth)++;

    HIL_NODE_GET_CLASS (value)->print (value, depth);
}

static void
hil_assignment_statement_class_init (HilAssignmentStatementClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_assignment_statement_init (HilAssignmentStatement *self)
{
    (void) self;
}

HilNode *
hil_assignment_statement_new (const gchar   *identifier,
                              HilExpression *value)
{
    HilAssignmentStatement *assignment_statement;

    g_return_val_if_fail (HIL_IS_EXPRESSION (value), NULL);

    assignment_statement = g_object_new (HIL_TYPE_ASSIGNMENT_STATEMENT, NULL);

    assignment_statement->identifier = g_strdup (identifier);
    assignment_statement->value = g_object_ref (value);

    return HIL_NODE (assignment_statement);
}
