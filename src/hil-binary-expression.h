/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "hil-expression.h"

#define HIL_TYPE_BINARY_EXPRESSION (hil_binary_expression_get_type ())

G_DECLARE_FINAL_TYPE (HilBinaryExpression, hil_binary_expression,
                      HIL, BINARY_EXPRESSION,
                      HilExpression)

typedef enum
{
    HIL_BINARY_OPERATOR_ADDITION,
    HIL_BINARY_OPERATOR_DIVISION,
    HIL_BINARY_OPERATOR_EQUALITY,
    HIL_BINARY_OPERATOR_GREATER_THAN,
    HIL_BINARY_OPERATOR_GREATER_THAN_OR_EQUAL,
    HIL_BINARY_OPERATOR_INEQUALITY,
    HIL_BINARY_OPERATOR_LOGICAL_CONJUNCTION,
    HIL_BINARY_OPERATOR_LOGICAL_DISJUNCTION,
    HIL_BINARY_OPERATOR_LESS_THAN,
    HIL_BINARY_OPERATOR_LESS_THAN_OR_EQUAL,
    HIL_BINARY_OPERATOR_MULTIPLICATION,
    HIL_BINARY_OPERATOR_SUBTRACTION
} HilBinaryOperator;

HilExpression *hil_binary_expression_get_lhs_expression (HilBinaryExpression        *binary_expression);
HilExpression *hil_binary_expression_get_rhs_expression (HilBinaryExpression        *binary_expression);

gint           hil_binary_expression_get_operator       (HilBinaryExpression        *binary_expression);

HilNode       *hil_binary_expression_new                (HilBinaryOperator           binary_operator,
                                                         HilExpression              *lhs_expression,
                                                         HilExpression              *rhs_expression,
                                                         struct _HilSourceReference *source_reference);
