/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#define HIL_TYPE_NODE (hil_node_get_type ())

G_DECLARE_DERIVABLE_TYPE (HilNode, hil_node, HIL, NODE, GObject)

struct _HilSourceReference;
struct _HilVisitor;

struct _HilNodeClass
{
    GObjectClass parent_class;

    void (*accept)          (HilNode *node,
                             struct _HilVisitor *visitor);
    void (*accept_children) (HilNode            *node,
                             struct _HilVisitor *visitor);
    void (*print)           (HilNode *node,
                             gsize   *depth);
};

struct _HilSourceReference *hil_node_get_source_reference (HilNode *node);

void                        hil_node_accept               (HilNode            *node,
                                                           struct _HilVisitor *visitor);
void                        hil_node_accept_children      (HilNode            *node,
                                                           struct _HilVisitor *visitor);

void                        hil_node_print                (HilNode *node);
