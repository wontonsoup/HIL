/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-expression-statement.h"

struct _HilExpressionStatement
{
    HilStatement parent_instance;

    HilExpression *expression;
};

G_DEFINE_TYPE (HilExpressionStatement, hil_expression_statement, HIL_TYPE_STATEMENT)

static void
finalize (GObject *object)
{
    HilExpressionStatement *expression_statement;

    expression_statement = HIL_EXPRESSION_STATEMENT (object);

    g_clear_object (&expression_statement->expression);

    G_OBJECT_CLASS (hil_expression_statement_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilExpressionStatement *expression_statement;
    HilNode *expression;

    expression_statement = HIL_EXPRESSION_STATEMENT (node);
    expression = HIL_NODE (expression_statement->expression);

    HIL_NODE_GET_CLASS (expression)->print (expression, depth);
}

static void
hil_expression_statement_class_init (HilExpressionStatementClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_expression_statement_init (HilExpressionStatement *self)
{
    (void) self;
}

HilNode *
hil_expression_statement_new (HilExpression *expression)
{
    HilExpressionStatement *expression_statement;

    g_return_val_if_fail (HIL_IS_EXPRESSION (expression), NULL);

    expression_statement = g_object_new (HIL_TYPE_EXPRESSION_STATEMENT, NULL);

    expression_statement->expression = g_object_ref (expression);

    return HIL_NODE (expression_statement);
}
