/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-compound-statement.h"

struct _HilCompoundStatement
{
    HilStatement parent_instance;

    GList *block_item_list;
};

G_DEFINE_TYPE (HilCompoundStatement, hil_compound_statement, HIL_TYPE_STATEMENT)

static void
finalize (GObject *object)
{
    HilCompoundStatement *compound_statement;

    compound_statement = HIL_COMPOUND_STATEMENT (object);

    g_list_free_full (compound_statement->block_item_list, g_object_unref);

    G_OBJECT_CLASS (hil_compound_statement_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilCompoundStatement *compound_statement;

    compound_statement = HIL_COMPOUND_STATEMENT (node);

    for (GList *i = compound_statement->block_item_list; i != NULL; i = i->next)
    {
        gsize current_depth;
        HilNode *statement;

        current_depth = *depth;
        statement = HIL_NODE (i->data);

        HIL_NODE_GET_CLASS (statement)->print (statement, &current_depth);
    }
}

static void
hil_compound_statement_class_init (HilCompoundStatementClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->finalize = finalize;

    node_class->print = print;
}

static void
hil_compound_statement_init (HilCompoundStatement *self)
{
    (void) self;
}

HilNode *
hil_compound_statement_new (GList *block_item_list)
{
    HilCompoundStatement *compound_statement;

    compound_statement = g_object_new (HIL_TYPE_COMPOUND_STATEMENT, NULL);

    compound_statement->block_item_list = block_item_list;

    return HIL_NODE (compound_statement);
}
