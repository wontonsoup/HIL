/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>
#include <glib-object.h>

#define HIL_TYPE_SOURCE_LOCATION (hil_source_location_get_type ())

G_DECLARE_FINAL_TYPE (HilSourceLocation, hil_source_location, HIL, SOURCE_LOCATION, GObject)

const gchar *hil_source_location_get_cursor (HilSourceLocation *location);
gsize        hil_source_location_get_line   (HilSourceLocation *location);
gsize        hil_source_location_get_column (HilSourceLocation *location);

HilSourceLocation *hil_source_location_new  (const gchar *cursor,
                                             gsize        line,
                                             gsize        column);
