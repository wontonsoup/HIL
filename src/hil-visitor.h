/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#define HIL_TYPE_VISITOR (hil_visitor_get_type ())

G_DECLARE_DERIVABLE_TYPE (HilVisitor, hil_visitor, HIL, VISITOR, GObject)

struct _HilBinaryExpression;
struct _HilBooleanLiteral;
struct _HilDeclaration;
struct _HilFunction;
struct _HilIntegerLiteral;
struct _HilRealLiteral;
struct _HilSourceFile;
struct _HilStringLiteral;
struct _HilVariable;

struct _HilVisitorClass
{
    GObjectClass parent_class;

    void (*visit_binary_expression)  (HilVisitor                  *visitor,
                                      struct _HilBinaryExpression *binary_expression);
    void (*visit_boolean_literal)    (HilVisitor                  *visitor,
                                      struct _HilBooleanLiteral   *boolean_literal);
    void (*visit_declaration)        (HilVisitor                  *visitor,
                                      struct _HilDeclaration      *declaration);
    void (*visit_function)           (HilVisitor                  *visitor,
                                      struct _HilFunction         *function);
    void (*visit_integer_literal)    (HilVisitor                  *visitor,
                                      struct _HilIntegerLiteral   *integer_literal);
    void (*visit_real_literal)       (HilVisitor                  *visitor,
                                      struct _HilRealLiteral      *real_literal);
    void (*visit_source_file)        (HilVisitor                  *visitor,
                                      struct _HilSourceFile       *source_file);
    void (*visit_string_literal)     (HilVisitor                  *visitor,
                                      struct _HilStringLiteral    *string_literal);
    void (*visit_variable)           (HilVisitor                  *visitor,
                                      struct _HilVariable         *variable);
};

void hil_visitor_visit_binary_expression (HilVisitor                  *visitor,
                                          struct _HilBinaryExpression *binary_expression);
void hil_visitor_visit_boolean_literal   (HilVisitor                  *visitor,
                                          struct _HilBooleanLiteral   *boolean_literal);
void hil_visitor_visit_declaration       (HilVisitor                  *visitor,
                                          struct _HilDeclaration      *declaration);
void hil_visitor_visit_function          (HilVisitor                  *visitor,
                                          struct _HilFunction         *function);
void hil_visitor_visit_integer_literal   (HilVisitor                  *visitor,
                                          struct _HilIntegerLiteral   *integer_literal);
void hil_visitor_visit_real_literal      (HilVisitor                  *visitor,
                                          struct _HilRealLiteral      *real_literal);
void hil_visitor_visit_source_file       (HilVisitor                  *visitor,
                                          struct _HilSourceFile       *source_file);
void hil_visitor_visit_string_literal    (HilVisitor                  *visitor,
                                          struct _HilStringLiteral    *string_literal);
void hil_visitor_visit_variable          (HilVisitor                  *visitor,
                                          struct _HilVariable         *variable);
