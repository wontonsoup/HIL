/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-real-type.h"

struct _HilRealType
{
    HilValueType parent_instance;
};

G_DEFINE_TYPE (HilRealType, hil_real_type, HIL_TYPE_VALUE_TYPE)

static gboolean
is_compatible_with (HilType *source_type,
                    HilType *target_type)
{
    return HIL_IS_REAL_TYPE (target_type);
}

static void
hil_real_type_class_init (HilRealTypeClass *klass)
{
    HilTypeClass *type_class;

    type_class = HIL_TYPE_CLASS (klass);

    type_class->is_compatible_with = is_compatible_with;
}

static void
hil_real_type_init (HilRealType *self)
{
    (void) self;
}

HilNode *
hil_real_type_new (void)
{
    return g_object_new (HIL_TYPE_REAL_TYPE, NULL);
}
