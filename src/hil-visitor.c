/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-visitor.h"

#include "hil-binary-expression.h"
#include "hil-boolean-literal.h"
#include "hil-function.h"
#include "hil-integer-literal.h"
#include "hil-real-literal.h"
#include "hil-source-file.h"
#include "hil-string-literal.h"
#include "hil-variable.h"

G_DEFINE_ABSTRACT_TYPE (HilVisitor, hil_visitor, G_TYPE_OBJECT)

static void
visit_binary_expression (HilVisitor          *visitor,
                         HilBinaryExpression *binary_expression)
{
    (void) visitor;
    (void) binary_expression;
}

static void
visit_boolean_literal (HilVisitor        *visitor,
                       HilBooleanLiteral *boolean_literal)
{
    (void) visitor;
    (void) boolean_literal;
}

static void
visit_declaration (HilVisitor     *visitor,
                   HilDeclaration *declaration)
{
    (void) visitor;
    (void) declaration;
}

static void
visit_function (HilVisitor  *visitor,
                HilFunction *function)
{
    (void) visitor;
    (void) function;
}

static void
visit_integer_literal (HilVisitor        *visitor,
                       HilIntegerLiteral *integer_literal)
{
    (void) visitor;
    (void) integer_literal;
}

static void
visit_source_file (HilVisitor    *visitor,
                   HilSourceFile *source_file)
{
    (void) visitor;
    (void) source_file;
}

static void
visit_string_literal (HilVisitor       *visitor,
                      HilStringLiteral *string_literal)
{
    (void) visitor;
    (void) string_literal;
}

static void
visit_variable (HilVisitor  *visitor,
                HilVariable *variable)
{
    (void) visitor;
    (void) variable;
}

static void
hil_visitor_class_init (HilVisitorClass *klass)
{
    klass->visit_binary_expression = visit_binary_expression;
    klass->visit_boolean_literal = visit_boolean_literal;
    klass->visit_declaration = visit_declaration;
    klass->visit_function = visit_function;
    klass->visit_integer_literal = visit_integer_literal;
    klass->visit_source_file = visit_source_file;
    klass->visit_string_literal = visit_string_literal;
    klass->visit_variable = visit_variable;
}

static void
hil_visitor_init (HilVisitor *self)
{
    (void) self;
}

void
hil_visitor_visit_binary_expression (HilVisitor          *visitor,
                                     HilBinaryExpression *binary_expression)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_BINARY_EXPRESSION (binary_expression));

    HIL_VISITOR_GET_CLASS (visitor)->visit_binary_expression (visitor, binary_expression);
}

void
hil_visitor_visit_boolean_literal (HilVisitor        *visitor,
                                   HilBooleanLiteral *boolean_literal)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_BOOLEAN_LITERAL (boolean_literal));

    HIL_VISITOR_GET_CLASS (visitor)->visit_boolean_literal (visitor, boolean_literal);
}

void
hil_visitor_visit_declaration (HilVisitor     *visitor,
                               HilDeclaration *declaration)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_DECLARATION (declaration));

    HIL_VISITOR_GET_CLASS (visitor)->visit_declaration (visitor, declaration);
}

void
hil_visitor_visit_function (HilVisitor  *visitor,
                            HilFunction *function)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_FUNCTION (function));

    HIL_VISITOR_GET_CLASS (visitor)->visit_function (visitor, function);
}

void
hil_visitor_visit_integer_literal (HilVisitor        *visitor,
                                   HilIntegerLiteral *integer_literal)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_INTEGER_LITERAL (integer_literal));

    HIL_VISITOR_GET_CLASS (visitor)->visit_integer_literal (visitor, integer_literal);
}

void
hil_visitor_visit_real_literal (HilVisitor     *visitor,
                                HilRealLiteral *real_literal)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_REAL_LITERAL (real_literal));

    HIL_VISITOR_GET_CLASS (visitor)->visit_real_literal (visitor, real_literal);
}

void
hil_visitor_visit_source_file (HilVisitor    *visitor,
                               HilSourceFile *source_file)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_SOURCE_FILE (source_file));

    HIL_VISITOR_GET_CLASS (visitor)->visit_source_file (visitor, source_file);
}

void
hil_visitor_visit_string_literal (HilVisitor       *visitor,
                                  HilStringLiteral *string_literal)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_STRING_LITERAL (string_literal));

    HIL_VISITOR_GET_CLASS (visitor)->visit_string_literal (visitor, string_literal);
}

void
hil_visitor_visit_variable (HilVisitor  *visitor,
                            HilVariable *variable)
{
    g_return_if_fail (HIL_IS_VISITOR (visitor));
    g_return_if_fail (HIL_IS_VARIABLE (variable));

    HIL_VISITOR_GET_CLASS (visitor)->visit_variable (visitor, variable);
}
