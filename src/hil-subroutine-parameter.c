/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-subroutine-parameter.h"

struct _HilSubroutineParameter
{
    GObject parent_instance;

    gchar *identifier;
    gchar *type;
};

G_DEFINE_TYPE (HilSubroutineParameter, hil_subroutine_parameter, G_TYPE_OBJECT)

static void
finalize (GObject *object)
{
    HilSubroutineParameter *subroutine_parameter;

    subroutine_parameter = HIL_SUBROUTINE_PARAMETER (object);

    g_clear_pointer (&subroutine_parameter->identifier, g_free);
    g_clear_pointer (&subroutine_parameter->type, g_free);

    G_OBJECT_CLASS (hil_subroutine_parameter_parent_class)->finalize (object);
}

static void
hil_subroutine_parameter_class_init (HilSubroutineParameterClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = finalize;
}

static void
hil_subroutine_parameter_init (HilSubroutineParameter *self)
{
    (void) self;
}

gchar *
hil_subroutine_parameter_get_identifier (HilSubroutineParameter *parameter)
{
    g_return_val_if_fail (HIL_IS_SUBROUTINE_PARAMETER (parameter), NULL);

    return g_strdup (parameter->identifier);
}

gchar *
hil_subroutine_parameter_get_parameter_type (HilSubroutineParameter *parameter)
{
    g_return_val_if_fail (HIL_IS_SUBROUTINE_PARAMETER (parameter), NULL);

    return g_strdup (parameter->type);
}

HilSubroutineParameter *
hil_subroutine_parameter_new (const gchar *identifier,
                              const gchar *type)
{
    HilSubroutineParameter *subroutine_parameter;

    subroutine_parameter = g_object_new (HIL_TYPE_SUBROUTINE_PARAMETER, NULL);

    subroutine_parameter->identifier = g_strdup (identifier);
    subroutine_parameter->type = g_strdup (type);

    return subroutine_parameter;
}
