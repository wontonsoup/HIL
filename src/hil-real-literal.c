/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-real-literal.h"

#include "hil-real-type.h"
#include "hil-source-reference.h"
#include "hil-visitor.h"

struct _HilRealLiteral
{
    HilLiteral parent_instance;
};

G_DEFINE_TYPE (HilRealLiteral, hil_real_literal, HIL_TYPE_LITERAL)

static void
accept (HilNode    *node,
        HilVisitor *visitor)
{
    hil_visitor_visit_real_literal (visitor, HIL_REAL_LITERAL (node));
}

static void
hil_real_literal_class_init (HilRealLiteralClass *klass)
{
    HilNodeClass *node_class;

    node_class = HIL_NODE_CLASS (klass);

    node_class->accept = accept;
}

static void
hil_real_literal_init (HilRealLiteral *self)
{
    (void) self;
}

HilNode *
hil_real_literal_new (const gchar        *value,
                      HilSourceReference *source_reference)
{
    HilRealLiteral *real_literal;

    real_literal = g_object_new (HIL_TYPE_REAL_LITERAL,
                                 "value", value,
                                 "expression-type", hil_real_type_new (),
                                 "source-reference", source_reference,
                                 NULL);

    return HIL_NODE (real_literal);
}
