/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-lexer.h"

#include "hil-source-location.h"
#include "hil-source-reference.h"

#include <ctype.h>
#include <string.h>

struct _HilLexer
{
    HilVisitor parent_instance;

    HilSourceFile *source_file;

    const gchar *file_cursor;
    const gchar *file_end;

    gsize line;
    gsize column;
};

G_DEFINE_TYPE (HilLexer, hil_lexer, HIL_TYPE_VISITOR)

static void
finalize (GObject *object)
{
    HilLexer *lexer;

    lexer = HIL_LEXER (object);

    lexer->source_file = NULL;
    lexer->file_cursor = NULL;
    lexer->file_end = NULL;
    lexer->line = 0;
    lexer->column = 0;

    G_OBJECT_CLASS (hil_lexer_parent_class)->finalize (object);
}

static gboolean
skip_comment (HilLexer *lexer)
{
    gsize start_line;
    gsize start_column;

    if (lexer->file_cursor > lexer->file_end - 2)
    {
        return FALSE;
    }

    start_line = lexer->line;
    start_column = lexer->column;

    if (lexer->file_cursor[0] == '/' &&
        lexer->file_cursor[1] == '*')
    {
        while (!(lexer->file_cursor > lexer->file_end - 2))
        {
            switch (lexer->file_cursor[0])
            {
                case '\n':
                {
                    lexer->file_cursor++;
                    lexer->line++;
                    lexer->column = 1;
                }
                break;

                case '*':
                {
                    if (lexer->file_cursor[1] == '/')
                    {
                        lexer->file_cursor += 2;
                        lexer->column += 2;

                        return TRUE;
                    }
                }

                default:
                {
                    lexer->file_cursor++;
                    lexer->column++;
                }
                break;
            }
        }

        if (lexer->file_cursor >= lexer->file_end - 2)
        {
            g_autofree gchar *filename = NULL;

            filename = hil_source_file_get_name (lexer->source_file);

            g_printerr ("%s:%zu:%zu: error: unterminated comment",
                        filename, start_line, start_column);
        }
    }

    return FALSE;
}

static gboolean
skip_whitespace (HilLexer *lexer)
{
    if (lexer->file_cursor >= lexer->file_end)
    {
        return FALSE;
    }

    if (isspace (lexer->file_cursor[0]))
    {
        if (lexer->file_cursor[0] == '\n')
        {
            lexer->line++;
            lexer->column = 1;
        }
        else
        {
            lexer->column++;
        }

        lexer->file_cursor++;

        return TRUE;
    }

    return FALSE;
}

static HilTokenType
get_identifier_or_keyword (const gchar *cursor,
                           gsize        length)
{
    switch (length)
    {
        case 2:
        {
            switch (cursor[0])
            {
                case 'i':
                {
                    if (strncmp (cursor, "if", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_IF;
                    }

                    if (strncmp (cursor, "in", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_IN;
                    }
                }
                break;

                case 'o':
                {
                    if (strncmp (cursor, "or", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_OR;
                    }
                }
                break;

                default:
                {
                }
                break;
            }
        }
        break;

        case 3:
        {
            switch (cursor[0])
            {
                case 'a':
                {
                    if (strncmp (cursor, "and", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_AND;
                    }
                }
                break;

                case 'i':
                {
                    if (strncmp (cursor, "int", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_BUILT_IN_TYPE_INT;
                    }
                }
                break;

                case 'n':
                {
                    if (strncmp (cursor, "not", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_NOT;
                    }
                }
                break;

                default:
                {
                }
                break;
            }
        }
        break;

        case 4:
        {
            switch (cursor[0])
            {
                case 'e':
                {
                    if (strncmp (cursor, "else", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_ELSE;
                    }
                }
                break;

                case 't':
                {
                    if (strncmp (cursor, "true", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_LITERAL_BOOLEAN;
                    }
                }

                default:
                {
                }
                break;
            }
        }
        break;

        case 5:
        {
            switch (cursor[0])
            {
                case 'b':
                {
                    if (strncmp (cursor, "break", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_BREAK;
                    }
                }
                break;

                case 'f':
                {
                    if (strncmp (cursor, "false", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_LITERAL_BOOLEAN;
                    }
                }

                case 'w':
                {
                    if (strncmp (cursor, "while", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_WHILE;
                    }
                }
                break;

                default:
                {
                }
                break;
            }
        }
        break;

        case 6:
        {
            switch (cursor[0])
            {
                case 'd':
                {
                    if (strncmp (cursor, "double", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_BUILT_IN_TYPE_DOUBLE;
                    }
                }
                break;

                case 'r':
                {
                    if (strncmp (cursor, "return", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_RETURN;
                    }
                }
                break;

                case 's':
                {
                    if (strncmp (cursor, "string", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_BUILT_IN_TYPE_STRING;
                    }
                }
                break;

                default:
                {
                };
            }
        };

        case 8:
        {
            switch (cursor[0])
            {
                case 'c':
                {
                    if (strncmp (cursor, "continue", length) == 0)
                    {
                        return HIL_TOKEN_TYPE_KEYWORD_CONTINUE;
                    }
                }
                break;

                default:
                {
                }
                break;
            }
        }
        break;

        default:
        {
        }
        break;
    }

    return HIL_TOKEN_TYPE_IDENTIFIER;
}

static HilToken *
read_next_token (HilLexer *lexer)
{
    g_autofree gchar *filename = NULL;
    g_autoptr (HilSourceLocation) token_start = NULL;
    HilTokenType token_type = HIL_TOKEN_TYPE_UNKNOWN;
    g_autoptr (HilSourceLocation) token_end = NULL;
    g_autoptr (HilSourceReference) source_reference = NULL;

    g_return_val_if_fail (HIL_IS_LEXER (lexer), NULL);

    filename = hil_source_file_get_name (lexer->source_file);
    token_start = hil_source_location_new (lexer->file_cursor,
                                           lexer->line, lexer->column);

    if (lexer->file_cursor >= lexer->file_end)
    {
        token_type = HIL_TOKEN_TYPE_EOF;
    }
    else if (isdigit (lexer->file_cursor[0]))
    {
        const gchar *start;

        start = lexer->file_cursor;

        lexer->file_cursor++;
        lexer->column++;

        if (lexer->file_cursor[0] == '.')
        {
            lexer->file_cursor++;
            lexer->column++;

            while (isdigit (lexer->file_cursor[0]))
            {
                lexer->file_cursor++;
                lexer->column++;
            }

            token_type = HIL_TOKEN_TYPE_LITERAL_REAL;
        }
        else
        {
            while (isdigit (lexer->file_cursor[0]))
            {
                lexer->file_cursor++;
                lexer->column++;
            }

            token_type = HIL_TOKEN_TYPE_LITERAL_INTEGER;
        }
    }
    else if (isalpha (lexer->file_cursor[0]) || lexer->file_cursor[0] == '_')
    {
        const gchar *start;
        gsize length;

        start = lexer->file_cursor;
        length = 0;

        while (isalnum (lexer->file_cursor[0]) || lexer->file_cursor[0] == '_')
        {
            lexer->file_cursor++;
            lexer->column++;
            length++;
        }

        token_type = get_identifier_or_keyword (start, length);
    }
    else
    {
        switch (lexer->file_cursor[0])
        {
            case '>':
            {
                lexer->file_cursor++;
                lexer->column++;

                if (lexer->file_cursor < lexer->file_end &&
                    lexer->file_cursor[0] == '=')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_OPERATOR_GE;
                }
                else
                {
                    token_type = HIL_TOKEN_TYPE_OPERATOR_GT;
                }
            }
            break;

            case '<':
            {
                lexer->file_cursor++;
                lexer->column++;

                if (lexer->file_cursor < lexer->file_end &&
                    lexer->file_cursor[0] == '=')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_OPERATOR_LE;
                }
                else
                {
                    token_type = HIL_TOKEN_TYPE_OPERATOR_LT;
                }
            }
            break;

            case '\'':
            {
                gsize start_column;

                start_column = lexer->column;

                lexer->file_cursor++;
                lexer->column++;

                while (lexer->file_cursor < lexer->file_end &&
                       lexer->file_cursor[0] != '\'' &&
                       lexer->file_cursor[0] != '\n')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    if (lexer->file_cursor < lexer->file_end &&
                        lexer->file_cursor[0] == '\\')
                    {
                        lexer->file_cursor++;
                        lexer->column++;

                        switch (lexer->file_cursor[0])
                        {
                            case '\'':
                            case '\\':
                            case 'n':
                            case 'r':
                            case 't':
                            {
                                lexer->file_cursor++;
                                lexer->column++;
                            }
                            break;

                            default:
                            {
                                g_printerr ("%s:%zu:%zu: error: unrecognized escape sequence",
                                            filename, lexer->line, lexer->column);
                            }
                            break;
                        }
                    }
                }

                if (lexer->file_cursor[0] == '\'')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_LITERAL_STRING;
                }
                else
                {
                    g_printerr ("%s:%zu:%zu: error: unterminated string literal",
                                filename, lexer->line, start_column);
                }
            }
            break;

            case ';':
            {
                lexer->file_cursor++;
                lexer->column++;

                token_type = HIL_TOKEN_TYPE_SEMICOLON;
            }
            break;

            case '{':
            {
                lexer->file_cursor++;
                lexer->column++;

                token_type = HIL_TOKEN_TYPE_BRACE_OPENING;
            }
            break;

            case '}':
            {
                lexer->file_cursor++;
                lexer->column++;

                token_type = HIL_TOKEN_TYPE_BRACE_CLOSING;
            }
            break;

            case '!':
            {
                lexer->file_cursor++;
                lexer->column++;

                if (lexer->file_cursor < lexer->file_end &&
                    lexer->file_cursor[0] == '=')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_OPERATOR_NEQ;
                }
            }
            break;

            case '=':
            {
                lexer->file_cursor++;
                lexer->column++;

                if (lexer->file_cursor < lexer->file_end &&
                    lexer->file_cursor[0] == '=')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_OPERATOR_EQ;
                }
                else
                {
                    token_type = HIL_TOKEN_TYPE_ASSIGN;
                }
            }
            break;

            case '(':
            {
                lexer->file_cursor++;
                lexer->column++;

                token_type = HIL_TOKEN_TYPE_PARENTHESIS_OPENING;
            }
            break;

            case ')':
            {
                lexer->file_cursor++;
                lexer->column++;

                token_type = HIL_TOKEN_TYPE_PARENTHESIS_CLOSING;
            }
            break;

            case ',':
            {
                lexer->file_cursor++;
                lexer->column++;

                token_type = HIL_TOKEN_TYPE_COMMA;
            }
            break;

            case ':':
            {
                lexer->file_cursor++;
                lexer->column++;

                token_type = HIL_TOKEN_TYPE_COLON;
            }
            break;

            case '*':
            {
                lexer->file_cursor++;
                lexer->column++;

                if (lexer->file_cursor < lexer->file_end &&
                    lexer->file_cursor[0] == '=')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_ASSIGN_MUL;
                }
                else
                {
                    token_type = HIL_TOKEN_TYPE_ASTERISK;
                }
            }
            break;

            case '/':
            {
                lexer->file_cursor++;
                lexer->column++;

                if (lexer->file_cursor < lexer->file_end &&
                    lexer->file_cursor[0] == '=')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_ASSIGN_DIV;
                }
                else
                {
                    token_type = HIL_TOKEN_TYPE_SLASH;
                }
            }
            break;

            case '+':
            {
                lexer->file_cursor++;
                lexer->column++;

                if (lexer->file_cursor < lexer->file_end &&
                    lexer->file_cursor[0] == '=')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_ASSIGN_ADD;
                }
                else
                {
                    token_type = HIL_TOKEN_TYPE_PLUS;
                }
            }
            break;

            case '-':
            {
                lexer->file_cursor++;
                lexer->column++;

                if (lexer->file_cursor < lexer->file_end &&
                    lexer->file_cursor[0] == '=')
                {
                    lexer->file_cursor++;
                    lexer->column++;

                    token_type = HIL_TOKEN_TYPE_ASSIGN_SUB;
                }
                else
                {
                    token_type = HIL_TOKEN_TYPE_MINUS;
                }
            }
            break;

            default:
            {
                lexer->file_cursor++;
                lexer->column++;
            }
            break;
        }
    }

    if (token_type == HIL_TOKEN_TYPE_UNKNOWN)
    {
        g_printerr ("%s:%zu:%zu: error: unrecognized token",
                    filename, lexer->line, lexer->column);
    }

    token_end = hil_source_location_new (lexer->file_cursor, lexer->line, lexer->column);
    source_reference = hil_source_reference_new (lexer->source_file,
                                                 token_start, token_end);

    return hil_token_new (token_type, source_reference);
}

static void
visit_source_file (HilVisitor    *visitor,
                   HilSourceFile *source_file)
{
    HilLexer *lexer;

    lexer = HIL_LEXER (visitor);

    lexer->source_file = source_file;
    lexer->file_cursor = hil_source_file_get_contents (lexer->source_file);
    lexer->file_end = lexer->file_cursor + hil_source_file_get_length (lexer->source_file);

    for (; ; )
    {
        g_autoptr (HilToken) token = NULL;

        while (skip_comment (lexer) || skip_whitespace (lexer));

        token = read_next_token (lexer);
        if (token == NULL)
        {
            break;
        }

        if (hil_token_get_token_type (token) == HIL_TOKEN_TYPE_EOF)
        {
            break;
        }

        hil_source_file_add_token (source_file, token);
    }
}

static void
hil_lexer_class_init (HilLexerClass *klass)
{
    GObjectClass *object_class;
    HilVisitorClass *visitor_class;

    object_class = G_OBJECT_CLASS (klass);
    visitor_class = HIL_VISITOR_CLASS (klass);

    object_class->finalize = finalize;

    visitor_class->visit_source_file = visit_source_file;
}

static void
hil_lexer_init (HilLexer *self)
{
    self->file_cursor = NULL;
    self->file_end = NULL;

    self->line = 1;
    self->column = 1;
}

void
hil_lexer_lex (HilLexer      *lexer,
               HilSourceFile *source_file)
{
    g_return_if_fail (HIL_IS_LEXER (lexer));

    hil_source_file_accept (source_file, HIL_VISITOR (lexer));

    {
        GList *tokens;
        g_autofree gchar *filename = NULL;

        tokens = hil_source_file_get_tokens (source_file);
        if (tokens == NULL)
        {
            return;
        }
        filename = hil_source_file_get_name (source_file);

        for (GList *i = tokens; i != NULL; i = i->next)
        {
            g_autofree gchar *token_string = NULL;

            token_string = hil_token_to_string (i->data);

            g_print ("%s\n", token_string);
        }
    }
}

HilLexer *
hil_lexer_new (void)
{
    return g_object_new (HIL_TYPE_LEXER, NULL);
}
