/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-declaration.h"

#include "hil-type.h"
#include "hil-visitor.h"

typedef struct
{
    gchar *identifier;
    HilType *type;
} HilDeclarationPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (HilDeclaration, hil_declaration,
                                     HIL_TYPE_NODE)

enum
{
    PROP_IDENTIFIER = 1,
    PROP_TYPE,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL };

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    HilDeclaration *declaration;
    HilDeclarationPrivate *priv;

    declaration = HIL_DECLARATION (object);
    priv = hil_declaration_get_instance_private (declaration);

    switch (property_id)
    {
        case PROP_IDENTIFIER:
        {
            g_clear_pointer (&priv->identifier, g_free);
            priv->identifier = g_value_dup_string (value);
        }
        break;

        case PROP_TYPE:
        {
            g_clear_object (&priv->type);
            priv->type = g_value_dup_object (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
    HilDeclaration *declaration;
    HilDeclarationPrivate *priv;

    declaration = HIL_DECLARATION (object);
    priv = hil_declaration_get_instance_private (declaration);

    switch (property_id)
    {
        case PROP_IDENTIFIER:
        {
            g_value_set_string (value, priv->identifier);
        }
        break;

        case PROP_TYPE:
        {
            g_value_set_object (value, priv->type);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
finalize (GObject *object)
{
    HilDeclaration *declaration;
    HilDeclarationPrivate *priv;

    declaration = HIL_DECLARATION (object);
    priv = hil_declaration_get_instance_private (declaration);

    g_clear_pointer (&priv->identifier, g_free);
    g_clear_object (&priv->type);

    G_OBJECT_CLASS (hil_declaration_parent_class)->finalize (object);
}

static void
accept (HilNode    *node,
        HilVisitor *visitor)
{
    hil_visitor_visit_declaration (visitor, HIL_DECLARATION (node));
}

static void
hil_declaration_class_init (HilDeclarationClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->set_property = set_property;
    object_class->get_property = get_property;
    object_class->finalize = finalize;

    node_class->accept = accept;

    properties[PROP_IDENTIFIER] =
        g_param_spec_string ("identifier", "Identifier", "Identifier",
                             NULL,
                             G_PARAM_CONSTRUCT_ONLY |
                             G_PARAM_WRITABLE |
                             G_PARAM_STATIC_STRINGS);
    properties[PROP_TYPE] =
        g_param_spec_object ("type", "Type", "Type",
                             HIL_TYPE_TYPE,
                             G_PARAM_CONSTRUCT_ONLY |
                             G_PARAM_WRITABLE |
                             G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
hil_declaration_init (HilDeclaration *self)
{
    (void) self;
}

gchar *
hil_declaration_get_identifier (HilDeclaration *declaration)
{
    HilDeclarationPrivate *priv;

    g_return_val_if_fail (HIL_IS_DECLARATION (declaration), NULL);

    priv = hil_declaration_get_instance_private (declaration);

    return g_strdup (priv->identifier);
}

HilType *
hil_declaration_get_symbol_type (HilDeclaration *declaration)
{
    HilDeclarationPrivate *priv;

    g_return_val_if_fail (HIL_IS_DECLARATION (declaration), NULL);

    priv = hil_declaration_get_instance_private (declaration);

    return g_object_ref (priv->type);
}
