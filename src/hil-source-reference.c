/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-source-reference.h"

#include "hil-source-file.h"
#include "hil-source-location.h"

struct _HilSourceReference
{
    GObject parent_instance;

    HilSourceFile *source_file;

    HilSourceLocation *start_location;
    HilSourceLocation *end_location;
};

G_DEFINE_TYPE (HilSourceReference, hil_source_reference, G_TYPE_OBJECT)

static void
finalize (GObject *object)
{
    HilSourceReference *source_reference;

    source_reference = HIL_SOURCE_REFERENCE (object);

    g_clear_object (&source_reference->source_file);
    g_clear_object (&source_reference->start_location);
    g_clear_object (&source_reference->end_location);

    G_OBJECT_CLASS (hil_source_reference_parent_class)->finalize (object);
}

static void
hil_source_reference_class_init (HilSourceReferenceClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = finalize;
}

static void
hil_source_reference_init (HilSourceReference *self)
{
    (void) self;
}

HilSourceLocation *
hil_source_reference_get_end_location (HilSourceReference *source_reference)
{
    g_return_val_if_fail (HIL_IS_SOURCE_REFERENCE (source_reference), NULL);

    return g_object_ref (source_reference->end_location);
}

HilSourceFile *
hil_source_reference_get_source_file (HilSourceReference *source_reference)
{
    g_return_val_if_fail (HIL_IS_SOURCE_REFERENCE (source_reference), NULL);

    return g_object_ref (source_reference->source_file);
}

HilSourceLocation *
hil_source_reference_get_start_location (HilSourceReference *source_reference)
{
    g_return_val_if_fail (HIL_IS_SOURCE_REFERENCE (source_reference), NULL);

    return g_object_ref (source_reference->start_location);
}

gchar *
hil_source_reference_to_string (HilSourceReference *source_reference)
{
    g_autofree gchar *filename = NULL;

    g_return_val_if_fail (HIL_IS_SOURCE_REFERENCE (source_reference), NULL);


    filename = hil_source_file_get_name (source_reference->source_file);

    return g_strdup_printf ("%s:%zu:%zu – %zu:%zu",
                            filename,
                            hil_source_location_get_line (source_reference->start_location),
                            hil_source_location_get_column (source_reference->start_location),
                            hil_source_location_get_line (source_reference->end_location),
                            hil_source_location_get_column (source_reference->end_location));
}

HilSourceReference *
hil_source_reference_new (HilSourceFile     *source_file,
                          HilSourceLocation *start_location,
                          HilSourceLocation *end_location)
{
    HilSourceReference *source_reference;

    g_return_val_if_fail (HIL_IS_SOURCE_FILE (source_file), NULL);
    g_return_val_if_fail (HIL_IS_SOURCE_LOCATION (start_location), NULL);
    g_return_val_if_fail (HIL_IS_SOURCE_LOCATION (end_location), NULL);

    source_reference = g_object_new (HIL_TYPE_SOURCE_REFERENCE, NULL);

    source_reference->source_file = g_object_ref (source_file);
    source_reference->start_location = g_object_ref (start_location);
    source_reference->end_location = g_object_ref (end_location);

    return source_reference;
}
