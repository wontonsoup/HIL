/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-boolean-literal.h"

#include "hil-boolean-type.h"
#include "hil-source-reference.h"
#include "hil-visitor.h"

struct _HilBooleanLiteral
{
    HilLiteral parent_instance;
};

G_DEFINE_TYPE (HilBooleanLiteral, hil_boolean_literal, HIL_TYPE_LITERAL)

static void
accept (HilNode    *node,
        HilVisitor *visitor)
{
    hil_visitor_visit_boolean_literal (visitor, HIL_BOOLEAN_LITERAL (node));
}

static void
hil_boolean_literal_class_init (HilBooleanLiteralClass *klass)
{
    HilNodeClass *node_class;

    node_class = HIL_NODE_CLASS (klass);

    node_class->accept = accept;
}

static void
hil_boolean_literal_init (HilBooleanLiteral *self)
{
    (void) self;
}

HilNode *
hil_boolean_literal_new (const gchar        *value,
                         HilSourceReference *source_reference)
{
    HilBooleanLiteral *boolean_literal;

    boolean_literal = g_object_new (HIL_TYPE_BOOLEAN_LITERAL,
                                    "value", value,
                                    "expression-type", hil_boolean_type_new (),
                                    "source-reference", source_reference,
                                    NULL);

    return HIL_NODE (boolean_literal);
}
