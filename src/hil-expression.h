/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "hil-node.h"

#define HIL_TYPE_EXPRESSION (hil_expression_get_type ())

G_DECLARE_DERIVABLE_TYPE (HilExpression, hil_expression, HIL, EXPRESSION, HilNode)

struct _HilExpressionClass
{
    HilNodeClass parent_class;
};

struct _HilValueType;

struct _HilValueType *hil_expression_get_expression_type (HilExpression *expression);
