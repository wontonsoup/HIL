/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "hil-statement.h"

#define HIL_TYPE_CONDITIONAL_STATEMENT (hil_conditional_statement_get_type ())

G_DECLARE_DERIVABLE_TYPE (HilConditionalStatement, hil_conditional_statement,
                          HIL, CONDITIONAL_STATEMENT,
                          HilStatement)

struct _HilConditionalStatementClass
{
    HilStatementClass parent_class;
};

HilNode *hil_conditional_statement_get_condition (HilConditionalStatement *conditional_statement);
