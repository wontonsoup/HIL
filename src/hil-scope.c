/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-scope.h"

#include "hil-declaration.h"

struct _HilScope
{
    GObject parent_instance;

    HilScope *parent_scope;

    GHashTable *symbols;
};

G_DEFINE_TYPE (HilScope, hil_scope, G_TYPE_OBJECT)

static void
finalize (GObject *object)
{
    HilScope *scope;

    scope = HIL_SCOPE (object);

    g_clear_object (&scope->parent_scope);
    g_hash_table_destroy (scope->symbols);

    G_OBJECT_CLASS (hil_scope_parent_class)->finalize (object);
}

static void
hil_scope_class_init (HilScopeClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = finalize;
}

static void
hil_scope_init (HilScope *self)
{
    self->parent_scope = NULL;
    self->symbols = g_hash_table_new_full (g_str_hash, g_str_equal,
                                           g_free, g_object_unref);
}

HilDeclaration *
hil_scope_look_up (HilScope    *scope,
                   const gchar *symbol_name)
{
    HilDeclaration *symbol;

    g_return_val_if_fail (HIL_IS_SCOPE (scope), NULL);
    g_return_val_if_fail (symbol_name != NULL, NULL);

    symbol = g_hash_table_lookup (scope->symbols, symbol_name);
    if (symbol != NULL)
    {
        return g_object_ref (symbol);
    }

    if (scope->parent_scope == NULL)
    {
        return NULL;
    }

    return hil_scope_look_up (scope->parent_scope, symbol_name);
}

gboolean
hil_scope_add_symbol (HilScope       *scope,
                      HilDeclaration *symbol)
{
    g_autofree gchar *identifier = NULL;

    g_return_val_if_fail (HIL_IS_SCOPE (scope), TRUE);
    g_return_val_if_fail (HIL_IS_DECLARATION (symbol), TRUE);

    identifier = hil_declaration_get_identifier (symbol);

    if (g_hash_table_lookup_extended (scope->symbols, identifier, NULL, NULL))
    {
        return FALSE;
    }

    return g_hash_table_insert (scope->symbols,
                                g_strdup (identifier), symbol);
}

HilScope *
hil_scope_new (HilScope *parent_scope)
{
    HilScope *scope;

    if (parent_scope != NULL)
    {
        g_return_val_if_fail (HIL_IS_SCOPE (parent_scope), NULL);
    }

    scope = g_object_new (HIL_TYPE_SCOPE, NULL);

    if (parent_scope != NULL)
    {
        scope->parent_scope = g_object_ref (parent_scope);
    }

    return scope;
}
