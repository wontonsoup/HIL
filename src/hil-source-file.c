/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-source-file.h"

#include "hil-node.h"
#include "hil-token.h"
#include "hil-visitor.h"

#include <gio/gio.h>

struct _HilSourceFile
{
    GObject parent_instance;

    gchar *filename;

    GMappedFile *mapped_file;

    GList *nodes;
    GList *tokens;
    GArray *code;
};

G_DEFINE_TYPE (HilSourceFile, hil_source_file, G_TYPE_OBJECT)

static void
finalize (GObject *object)
{
    HilSourceFile *source_file;

    source_file = HIL_SOURCE_FILE (object);

    g_clear_pointer (&source_file->filename, g_free);
    g_clear_pointer (&source_file->mapped_file, g_mapped_file_unref);

    g_list_free_full (source_file->nodes, g_object_unref);
    g_list_free_full (source_file->tokens, g_object_unref);
    g_array_unref (source_file->code);

    G_OBJECT_CLASS (hil_source_file_parent_class)->finalize (object);
}

static void
hil_source_file_class_init (HilSourceFileClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = finalize;
}

static void
hil_source_file_init (HilSourceFile *self)
{
    self->nodes = NULL;
    self->tokens = NULL;
    self->code = g_array_new (TRUE, FALSE, sizeof (gpointer));
}

void
hil_source_file_accept (HilSourceFile *source_file,
                        HilVisitor    *visitor)
{
    g_return_if_fail (HIL_IS_SOURCE_FILE (source_file));
    g_return_if_fail (HIL_IS_VISITOR (visitor));

    hil_visitor_visit_source_file (visitor, source_file);
}

void
hil_source_file_accept_children (HilSourceFile *source_file,
                                 HilVisitor    *visitor)
{
    g_return_if_fail (HIL_IS_SOURCE_FILE (source_file));
    g_return_if_fail (HIL_IS_VISITOR (visitor));

    for (GList *i = source_file->nodes; i != NULL; i = i->next)
    {
        hil_node_accept (i->data, visitor);
    }
}

void
hil_source_file_emit (HilSourceFile *source_file,
                      guchar         instruction)
{
    g_return_if_fail (HIL_IS_SOURCE_FILE (source_file));

    g_array_append_val (source_file->code, instruction);
}

void
hil_source_file_add_node (HilSourceFile *source_file,
                          HilNode       *node)
{
    g_return_if_fail (HIL_IS_SOURCE_FILE (source_file));
    g_return_if_fail (HIL_IS_NODE (node));

    source_file->nodes = g_list_append (source_file->nodes,
                                        g_object_ref (node));
}

void
hil_source_file_add_token (HilSourceFile *source_file,
                           HilToken      *token)
{
    g_return_if_fail (HIL_IS_SOURCE_FILE (source_file));
    g_return_if_fail (HIL_IS_TOKEN (token));

    source_file->tokens = g_list_append (source_file->tokens,
                                         g_object_ref (token));
}

const gchar *
hil_source_file_get_contents (HilSourceFile *source_file)
{
    g_return_val_if_fail (HIL_IS_SOURCE_FILE (source_file), NULL);

    return g_mapped_file_get_contents (source_file->mapped_file);
}

gsize
hil_source_file_get_length (HilSourceFile *source_file)
{
    g_return_val_if_fail (HIL_IS_SOURCE_FILE (source_file), 0);

    return g_mapped_file_get_length (source_file->mapped_file);
}

gchar *
hil_source_file_get_name (HilSourceFile *source_file)
{
    g_return_val_if_fail (HIL_IS_SOURCE_FILE (source_file), NULL);

    return g_strdup (source_file->filename);
}

GList *
hil_source_file_get_nodes (HilSourceFile *source_file)
{
    g_return_val_if_fail (HIL_IS_SOURCE_FILE (source_file), NULL);

    return source_file->nodes;
}

GList *
hil_source_file_get_tokens (HilSourceFile *source_file)
{
    g_return_val_if_fail (HIL_IS_SOURCE_FILE (source_file), NULL);

    return source_file->tokens;
}

HilSourceFile *
hil_source_file_new (const gchar *filename)
{
    g_autoptr (HilSourceFile) source_file = NULL;
    g_autoptr (GError) error = NULL;

    g_return_val_if_fail (filename != NULL, NULL);

    source_file = g_object_new (HIL_TYPE_SOURCE_FILE, NULL);

    source_file->filename = g_strdup (filename);
    source_file->mapped_file = g_mapped_file_new (filename, FALSE, &error);

    if (source_file->mapped_file == NULL)
    {
        return NULL;
    }

    return g_object_ref (source_file);
}
