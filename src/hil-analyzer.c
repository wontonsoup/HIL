/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-analyzer.h"

#include "hil-binary-expression.h"
#include "hil-expression-private.h"
#include "hil-function.h"
#include "hil-scope.h"
#include "hil-source-file.h"
#include "hil-source-reference.h"
#include "hil-value-type.h"
#include "hil-variable.h"

#include <stdlib.h>

struct _HilAnalyzer
{
    HilVisitor parent_instance;

    GQueue *scope_queue;
};

G_DEFINE_TYPE (HilAnalyzer, hil_analyzer, HIL_TYPE_VISITOR)

static void
finalize (GObject *object)
{
    G_OBJECT_CLASS (hil_analyzer_parent_class)->finalize (object);
}

static void
visit_binary_expression (HilVisitor          *visitor,
                         HilBinaryExpression *binary_expression)
{
    g_autoptr (HilExpression) lhs_expression = NULL;
    g_autoptr (HilExpression) rhs_expression = NULL;
    g_autoptr (HilValueType) lhs_type = NULL;
    g_autoptr (HilValueType) rhs_type = NULL;

    lhs_expression = hil_binary_expression_get_lhs_expression (binary_expression);
    rhs_expression = hil_binary_expression_get_rhs_expression (binary_expression);

    hil_node_accept_children (HIL_NODE (binary_expression), visitor);

    lhs_type = hil_expression_get_expression_type (lhs_expression);
    rhs_type = hil_expression_get_expression_type (rhs_expression);

    if (hil_type_is_compatible_with (HIL_TYPE (lhs_type), HIL_TYPE (rhs_type)))
    {
        hil_expression_set_type (HIL_EXPRESSION (binary_expression), lhs_type);
    }
    else
    {
        g_autoptr (HilSourceReference) source_reference = NULL;
        g_autofree gchar *source_reference_string = NULL;

        source_reference = hil_node_get_source_reference (HIL_NODE (binary_expression));
        source_reference_string = hil_source_reference_to_string (source_reference);

        g_printerr ("%s: error: incompatible types in binary expression: %s and %s\n",
                    source_reference_string,
                    g_type_name (G_TYPE_FROM_INSTANCE (lhs_type)),
                    g_type_name (G_TYPE_FROM_INSTANCE (rhs_type)));

        exit (1);
    }
}

static void
visit_declaration (HilVisitor     *visitor,
                   HilDeclaration *declaration)
{
    HilAnalyzer *analyzer;
    HilScope *scope;

    analyzer = HIL_ANALYZER (visitor);
    scope = g_queue_peek_head (analyzer->scope_queue);

    if (!hil_scope_add_symbol (scope, declaration))
    {
        g_autoptr (HilSourceReference) source_reference = NULL;
        g_autofree gchar *source_reference_string = NULL;
        g_autofree gchar *identifier = NULL;
        HilDeclaration *previous_declaration;
        g_autoptr (HilSourceReference) previous_source_reference = NULL;
        g_autofree gchar *previous_source_reference_string = NULL;

        source_reference = hil_node_get_source_reference (HIL_NODE (declaration));
        source_reference_string = hil_source_reference_to_string (source_reference);
        identifier = hil_declaration_get_identifier (declaration);
        previous_declaration = hil_scope_look_up (scope, identifier);
        previous_source_reference = hil_node_get_source_reference (HIL_NODE (previous_declaration));
        previous_source_reference_string = hil_source_reference_to_string (source_reference);

        g_printerr ("%s: error: redefinition of %s\n", source_reference_string, identifier);
        g_printerr ("Previous declaration here: %s\n", previous_source_reference_string);

        exit (1);
    }
}

static void
visit_function (HilVisitor  *visitor,
                HilFunction *function)
{
}

static void
visit_source_file (HilVisitor    *visitor,
                   HilSourceFile *source_file)
{
    hil_source_file_accept_children (source_file, visitor);
}

static void
visit_variable (HilVisitor  *visitor,
                HilVariable *variable)
{
    HilDeclaration *declaration;
    g_autoptr (HilType) type = NULL;
    g_autoptr (HilExpression) initializer = NULL;
    g_autoptr (HilValueType) initializer_type = NULL;

    declaration = HIL_DECLARATION (variable);
    type = hil_declaration_get_symbol_type (declaration);
    initializer = hil_variable_get_initializer (variable);

    hil_node_accept (HIL_NODE (initializer), visitor);

    initializer_type = hil_expression_get_expression_type (initializer);
    if (!hil_type_is_compatible_with (type, HIL_TYPE (initializer_type)))
    {
        g_autoptr (HilSourceReference) source_reference = NULL;
        g_autofree gchar *source_reference_string = NULL;

        source_reference = hil_node_get_source_reference (HIL_NODE (variable));
        source_reference_string = hil_source_reference_to_string (source_reference);

        g_printerr ("%s: error: initializing variable of type %s with %s\n",
                    source_reference_string,
                    g_type_name (G_TYPE_FROM_INSTANCE (type)),
                    g_type_name (G_TYPE_FROM_INSTANCE (initializer_type)));

        exit (1);
    }
}

static void
hil_analyzer_class_init (HilAnalyzerClass *klass)
{
    GObjectClass *object_class;
    HilVisitorClass *visitor_class;

    object_class = G_OBJECT_CLASS (klass);
    visitor_class = HIL_VISITOR_CLASS (klass);

    object_class->finalize = finalize;

    visitor_class->visit_binary_expression = visit_binary_expression;
    visitor_class->visit_declaration = visit_declaration;
    visitor_class->visit_function = visit_function;
    visitor_class->visit_source_file = visit_source_file;
    visitor_class->visit_variable = visit_variable;
}

static void
hil_analyzer_init (HilAnalyzer *self)
{
    self->scope_queue = g_queue_new ();

    g_queue_push_head (self->scope_queue, hil_scope_new (NULL));
}

void
hil_analyzer_analyze (HilAnalyzer *analyzer,
                      HilSourceFile     *source_file)
{
    hil_source_file_accept (source_file, HIL_VISITOR (analyzer));
}

HilAnalyzer *
hil_analyzer_new (void)
{
    return g_object_new (HIL_TYPE_ANALYZER, NULL);
}
