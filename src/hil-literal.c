/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-literal.h"

typedef struct
{
    gchar *value;
} HilLiteralPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (HilLiteral, hil_literal, HIL_TYPE_EXPRESSION)

enum
{
    PROP_VALUE = 1,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL };

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    HilLiteral *literal;
    HilLiteralPrivate *priv;

    literal = HIL_LITERAL (object);
    priv = hil_literal_get_instance_private (literal);

    switch (property_id)
    {
        case PROP_VALUE:
        {
            g_clear_pointer (&priv->value, g_free);
            priv->value = g_value_dup_string (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
    HilLiteral *literal;
    HilLiteralPrivate *priv;

    literal = HIL_LITERAL (object);
    priv = hil_literal_get_instance_private (literal);

    switch (property_id)
    {
        case PROP_VALUE:
        {
            g_value_set_string (value, priv->value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
finalize (GObject *object)
{
    HilLiteral *literal;
    HilLiteralPrivate *priv;

    literal = HIL_LITERAL (object);
    priv = hil_literal_get_instance_private (literal);

    g_clear_pointer (&priv->value, g_free);

    G_OBJECT_CLASS (hil_literal_parent_class)->finalize (object);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HilLiteral *literal;
    HilLiteralPrivate *priv;

    literal = HIL_LITERAL (node);
    priv = hil_literal_get_instance_private (literal);

    HIL_NODE_CLASS (hil_literal_parent_class)->print (node, depth);

    (*depth)++;

    for (gsize i = *depth; i > 0; i--)
    {
        g_print ("  ");
    }
    g_print ("Value: %s\n", priv->value);
}

static void
hil_literal_class_init (HilLiteralClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->set_property = set_property;
    object_class->get_property = get_property;
    object_class->finalize = finalize;

    node_class->print = print;

    properties[PROP_VALUE] = g_param_spec_string ("value", "Value", "Value",
                                                  NULL,
                                                  G_PARAM_CONSTRUCT_ONLY |
                                                  G_PARAM_READWRITE |
                                                  G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
hil_literal_init (HilLiteral *self)
{
    (void) self;
}

gchar *
hil_literal_get_value (HilLiteral *literal)
{
    HilLiteralPrivate *priv;

    g_return_val_if_fail (HIL_IS_LITERAL (literal), NULL);

    priv = hil_literal_get_instance_private (literal);

    return g_strdup (priv->value);
}
