/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-callable-type.h"

struct _HilCallableType
{
    HilType parent_instance;

    HilType *return_type;
};

G_DEFINE_TYPE (HilCallableType, hil_callable_type, HIL_TYPE_TYPE)

static void
finalize (GObject *object)
{
    HilCallableType *callable_type;

    callable_type = HIL_CALLABLE_TYPE (object);

    g_clear_object (&callable_type->return_type);
}

static void
hil_callable_type_class_init (HilCallableTypeClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = finalize;
}

static void
hil_callable_type_init (HilCallableType *self)
{
    (void) self;
}

HilNode *
hil_callable_type_new (HilType *return_type)
{
    HilCallableType *callable_type;

    g_return_val_if_fail (HIL_IS_TYPE (return_type), NULL);

    callable_type = g_object_new (HIL_TYPE_CALLABLE_TYPE, NULL);

    callable_type->return_type = g_object_ref (return_type);

    return HIL_NODE (callable_type);
}
