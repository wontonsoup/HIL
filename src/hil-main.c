/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-analyzer.h"
#include "hil-code-generator.h"
#include "hil-lexer.h"
#include "hil-parser.h"

#include <locale.h>
#include <stdlib.h>

#include <gio/gio.h>

int
main (int    argc,
      char **argv)
{
    g_autoptr (GOptionContext) option_context = NULL;
    gchar **files = NULL;
    const GOptionEntry option_entries[] =
    {
        {
            G_OPTION_REMAINING, 0, G_OPTION_FLAG_NONE,
            G_OPTION_ARG_FILENAME_ARRAY, &files, NULL, NULL
        },
        { NULL, 0, 0, 0, NULL, NULL, NULL }
    };

    setlocale (LC_ALL, "");

    option_context = g_option_context_new ("[FILE]");

    g_option_context_add_main_entries (option_context, option_entries, NULL);

    {
        g_autoptr (GError) error = NULL;

        if (!g_option_context_parse (option_context, &argc, &argv, &error))
        {
            g_print ("%s\n", error->message);

            return EXIT_FAILURE;
        }
    }

    if (files == NULL)
    {
        g_print ("No input file specified\n");

        return EXIT_FAILURE;
    }

    for (size_t i = 0; files[i] != NULL; files++)
    {
        g_autoptr (HilSourceFile) source_file = NULL;
        g_autoptr (HilLexer) lexer = NULL;
        g_autoptr (HilParser) parser = NULL;
        g_autoptr (HilAnalyzer) analyzer = NULL;
        g_autoptr (HilCodeGenerator) code_generator = NULL;

        source_file = hil_source_file_new (files[i]);
        lexer = hil_lexer_new ();
        parser = hil_parser_new ();
        analyzer = hil_analyzer_new ();
        code_generator = hil_code_generator_new ();

        g_debug ("Performing lexical analysis");
        hil_lexer_lex (lexer, source_file);
        g_debug ("Performing syntax analysis");
        hil_parser_parse (parser, source_file);
        g_debug ("Performing semantic analysis");
        hil_analyzer_analyze (analyzer, source_file);
        g_debug ("Emitting code");
        hil_code_generator_emit (code_generator, source_file);
    }

    return EXIT_SUCCESS;
}
