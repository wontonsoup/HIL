/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>
#include <glib-object.h>

#define HIL_TYPE_SCOPE (hil_scope_get_type ())

G_DECLARE_FINAL_TYPE (HilScope, hil_scope, HIL, SCOPE, GObject)

struct _HilDeclaration;

struct _HilDeclaration  *hil_scope_look_up    (HilScope               *scope,
                                               const gchar            *symbol_name);

gboolean                 hil_scope_add_symbol (HilScope               *scope,
                                               struct _HilDeclaration *symbol);

HilScope                *hil_scope_new        (HilScope               *parent_scope);
