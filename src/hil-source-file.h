/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#define HIL_TYPE_SOURCE_FILE (hil_source_file_get_type ())

struct _HilNode;
struct _HilToken;
struct _HilVisitor;

G_DECLARE_FINAL_TYPE (HilSourceFile, hil_source_file, HIL, SOURCE_FILE, GObject)

void           hil_source_file_accept          (HilSourceFile      *source_file,
                                                struct _HilVisitor *visitor);
void           hil_source_file_accept_children (HilSourceFile      *source_file,
                                                struct _HilVisitor *visitor);

void           hil_source_file_emit            (HilSourceFile      *source_file,
                                                guchar              instruction);
void           hil_source_file_emit2           (HilSourceFile      *source_file,
                                                guchar              instruction,
                                                GVariant           *operand);

void           hil_source_file_add_token       (HilSourceFile      *source_file,
                                                struct _HilToken   *token);
void           hil_source_file_add_node        (HilSourceFile      *source_file,
                                                struct _HilNode    *node);

const gchar   *hil_source_file_get_contents    (HilSourceFile      *source_file);
gsize          hil_source_file_get_length      (HilSourceFile      *source_file);
gchar         *hil_source_file_get_name        (HilSourceFile      *source_file);
GList         *hil_source_file_get_nodes       (HilSourceFile      *source_file);
GList         *hil_source_file_get_tokens      (HilSourceFile      *source_file);

HilSourceFile *hil_source_file_new             (const gchar        *filename);
