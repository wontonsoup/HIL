/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hil-expression.h"

#include "hil-expression-private.h"
#include "hil-value-type.h"

typedef struct
{
    HilValueType *type;
} HilExpressionPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (HilExpression, hil_expression, HIL_TYPE_NODE)

enum
{
    PROP_EXPRESSION_TYPE = 1,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL };

static void
set_property (GObject      *object,
              guint         property_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    HilExpression *expression;
    HilExpressionPrivate *priv;

    expression = HIL_EXPRESSION (object);
    priv = hil_expression_get_instance_private (expression);

    switch (property_id)
    {
        case PROP_EXPRESSION_TYPE:
        {
            g_clear_object (&priv->type);
            priv->type = g_value_dup_object (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
get_property (GObject    *object,
              guint       property_id,
              GValue     *value,
              GParamSpec *pspec)
{
    HilExpression *expression;
    HilExpressionPrivate *priv;

    expression = HIL_EXPRESSION (object);
    priv = hil_expression_get_instance_private (expression);

    switch (property_id)
    {
        case PROP_EXPRESSION_TYPE:
        {
            g_value_set_object (value, priv->type);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
finalize (GObject *object)
{
    HilExpression *expression;
    HilExpressionPrivate *priv;

    expression = HIL_EXPRESSION (object);
    priv = hil_expression_get_instance_private (expression);

    g_clear_object (&priv->type);
}

static void
print (HilNode *node,
       gsize   *depth)
{
    HIL_NODE_CLASS (hil_expression_parent_class)->print (node, depth);
}

static void
hil_expression_class_init (HilExpressionClass *klass)
{
    GObjectClass *object_class;
    HilNodeClass *node_class;

    object_class = G_OBJECT_CLASS (klass);
    node_class = HIL_NODE_CLASS (klass);

    object_class->set_property = set_property;
    object_class->get_property = get_property;
    object_class->finalize = finalize;

    node_class->print = print;

    properties[PROP_EXPRESSION_TYPE] =
        g_param_spec_object ("expression-type", "Expression type", "Expression type",
                             HIL_TYPE_VALUE_TYPE,
                             G_PARAM_CONSTRUCT_ONLY |
                             G_PARAM_READWRITE |
                             G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

static void
hil_expression_init (HilExpression *self)
{
    (void) self;
}

HilValueType *
hil_expression_get_expression_type (HilExpression *expression)
{
    HilExpressionPrivate *priv;

    g_return_val_if_fail (HIL_IS_EXPRESSION (expression), NULL);

    priv = hil_expression_get_instance_private (expression);

    if (priv->type == NULL)
    {
        return NULL;
    }

    return g_object_ref (priv->type);
}

/* Private methods */
void
hil_expression_set_type (HilExpression *expression,
                         HilValueType  *type)
{
    HilExpressionPrivate *priv;

    g_return_if_fail (HIL_IS_EXPRESSION (expression));
    g_return_if_fail (HIL_IS_VALUE_TYPE (type));

    priv = hil_expression_get_instance_private (expression);

    g_clear_object (&priv->type);
    priv->type = g_object_ref (type);
}
