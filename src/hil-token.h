/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of HIL.
 *
 * HIL is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * HIL is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HIL.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#define HIL_TYPE_TOKEN (hil_token_get_type ())

G_DECLARE_FINAL_TYPE (HilToken, hil_token, HIL, TOKEN, GObject)

typedef enum
{
    HIL_TOKEN_TYPE_UNKNOWN,

    HIL_TOKEN_TYPE_ASSIGN,
    HIL_TOKEN_TYPE_ASSIGN_ADD,
    HIL_TOKEN_TYPE_ASSIGN_DIV,
    HIL_TOKEN_TYPE_ASSIGN_MUL,
    HIL_TOKEN_TYPE_ASSIGN_SUB,
    HIL_TOKEN_TYPE_ASTERISK,
    HIL_TOKEN_TYPE_BRACE_CLOSING,
    HIL_TOKEN_TYPE_BRACE_OPENING,
    HIL_TOKEN_TYPE_COLON,
    HIL_TOKEN_TYPE_COMMA,
    HIL_TOKEN_TYPE_IDENTIFIER,
    HIL_TOKEN_TYPE_MINUS,
    HIL_TOKEN_TYPE_PARENTHESIS_CLOSING,
    HIL_TOKEN_TYPE_PARENTHESIS_OPENING,
    HIL_TOKEN_TYPE_PLUS,
    HIL_TOKEN_TYPE_SEMICOLON,
    HIL_TOKEN_TYPE_SLASH,

    HIL_TOKEN_TYPE_BUILT_IN_TYPE_BOOLEAN,
    HIL_TOKEN_TYPE_BUILT_IN_TYPE_DOUBLE,
    HIL_TOKEN_TYPE_BUILT_IN_TYPE_INT,
    HIL_TOKEN_TYPE_BUILT_IN_TYPE_STRING,

    HIL_TOKEN_TYPE_KEYWORD_AND,
    HIL_TOKEN_TYPE_KEYWORD_BREAK,
    HIL_TOKEN_TYPE_KEYWORD_CONTINUE,
    HIL_TOKEN_TYPE_KEYWORD_ELSE,
    HIL_TOKEN_TYPE_KEYWORD_IF,
    HIL_TOKEN_TYPE_KEYWORD_IN,
    HIL_TOKEN_TYPE_KEYWORD_NOT,
    HIL_TOKEN_TYPE_KEYWORD_OR,
    HIL_TOKEN_TYPE_KEYWORD_RETURN,
    HIL_TOKEN_TYPE_KEYWORD_WHILE,

    HIL_TOKEN_TYPE_LITERAL_BOOLEAN,
    HIL_TOKEN_TYPE_LITERAL_INTEGER,
    HIL_TOKEN_TYPE_LITERAL_REAL,
    HIL_TOKEN_TYPE_LITERAL_STRING,

    HIL_TOKEN_TYPE_OPERATOR_EQ,
    HIL_TOKEN_TYPE_OPERATOR_GE,
    HIL_TOKEN_TYPE_OPERATOR_GT,
    HIL_TOKEN_TYPE_OPERATOR_LE,
    HIL_TOKEN_TYPE_OPERATOR_LT,
    HIL_TOKEN_TYPE_OPERATOR_NEQ,

    HIL_TOKEN_TYPE_EOF,
} HilTokenType;

struct _HilSourceReference;

gboolean                    hil_token_type_is_built_in_type (HilTokenType                type);

const gchar                *hil_token_type_to_string        (HilTokenType                type);

HilTokenType                hil_token_get_token_type        (HilToken                   *token);
struct _HilSourceReference *hil_token_get_source_reference  (HilToken                   *token);
gchar                      *hil_token_get_value             (HilToken                   *token);

gchar                      *hil_token_to_string             (HilToken                   *token);

HilToken                   *hil_token_new                   (HilTokenType                type,
                                                             struct _HilSourceReference *source_reference);
